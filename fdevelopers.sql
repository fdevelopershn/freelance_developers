-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-07-2019 a las 23:22:06
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `fdevelopers`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `heading` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `title` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `author` varchar(55) NOT NULL,
  `image` text NOT NULL,
  `slug` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `blog`
--

INSERT INTO `blog` (`id`, `heading`, `title`, `content`, `author`, `image`, `slug`, `created_at`, `updated_at`) VALUES
(8, 'Tecnología 5G', '3 grandes ventajas que traerá la tecnología 5G', '<p>Recientemente se estren&oacute; la tecnolog&iacute;a 5G en Reino Unido, tambi&eacute;n ya funciona en paises como Corea del Sur y en algunos lugares de Estados Unidos. Operadoras como Samsung, Xiaomi, LG, Motorola, ZTE o Huawei se han puesto manos a la obra para adaptar sus equipos y ofrecer a los consumidores este tipo de conectividad.<br /><br />Con el 5G, es probable que el internet de tu celular sea m&aacute;s r&aacute;pido que el wifi de tu casa. Estamos hablando de tiempos que en lugar de minutos pasar&aacute;n a ser tan solo segundos.<br /><br /><strong>Los expertos hablan de las 3 ventajas que traer&aacute; la red m&oacute;vil 5G:&nbsp;</strong></p>\r\n<blockquote>\r\n<p><strong>1. Mayor velocidad</strong>: La velocidad del 4G es de 1 Gbps por segundo. La nueva red, en su m&aacute;ximo potencial, ser&aacute; capaz de ofrecer velocidades est&aacute;ndar de 20 Gbps por segundo, seg&uacute;n el regulador brit&aacute;nico Ofcom. Seg&uacute;n explica el diario The Wall Street Journal, descargar una playlist de Spotify completa de una hora de duraci&oacute;n supon&iacute;a 7 minutos con 3G, 20 segundos con 4G y 0,6 milisegundos con 5G. Si quieres llevarte una pel&iacute;cula al avi&oacute;n para verla mientras est&aacute;s offline, una posibilidad que ofrecen servicios como Amazon Premium o Netflix, tardar&aacute;s 3,7 segundos en descarg&aacute;rtela con 5G frente a los 2 minutos con 4G.</p>\r\n</blockquote>\r\n<blockquote>\r\n<p><strong>2. Ahorro de Bater&iacute;a:</strong> Qalcomm el fabricante de chips, ha prometido que los primeros modelos de tel&eacute;fonos con 5G la bater&iacute;a tendr&aacute; una \"vida &uacute;til de todo el d&iacute;a\". Los m&oacute;dems de segunda y tercera generaci&oacute;n ser&aacute;n inevitablemente m&aacute;s eficientes en el consumo de energ&iacute;a. Por lo tanto, permitir&aacute;n que los fabricantes de tel&eacute;fonos m&oacute;viles ofrezcan una vida &uacute;til m&aacute;s larga o smartphones m&aacute;s delgados. Este ahorro de bater&iacute;a llega porque la tecnolog&iacute;a 5G deja que sea la infraestructura 4G la que funcione, permaneciendo inactiva hasta que el usuario solicita procesos que exigen velocidades m&aacute;s r&aacute;pidas.</p>\r\n</blockquote>\r\n<blockquote>\r\n<p><strong>3. Cobertura:</strong> La infraestructura 5G permite que m&aacute;s dispositivos est&eacute;n conectados al mismo tiempo. Tiene m&aacute;s potencia y esto ayudar&aacute; salvar el cuello de botella electromagn&eacute;tico que existe en las grandes aglomeraciones, sobretodo urbanas. Esto pondr&iacute;a fin a las p&eacute;rdidas de cobertura en los grandes eventos, como conciertos, o como sucede en momentos puntuales, como fin de a&ntilde;o y otras celebraciones masivas. En teor&iacute;a, podr&aacute; soportar simult&aacute;neamente m&aacute;s de un mill&oacute;n de dispositivos por kil&oacute;metro cuadrado. Seg&uacute;n la Uni&oacute;n Internacional de Telecomunicaciones, se espera que la infraestructura d&eacute; soporte a edificios, hogares y ciudades inteligentes, realidad virtual, v&iacute;deos en 3D as&iacute; como trabajo y juegos en la nube o cirug&iacute;a a distancia. Tambi&eacute;n permitir&aacute; dar apoyo a los autos sin conductor.</p>\r\n</blockquote>\r\n<p><strong>Fuente: BBC</strong></p>', 'Gerson Alvarez', 'Tecnología 5G.webp', 'tecnologia-5g', '2019-06-14 17:54:42', '2019-07-15 20:02:51'),
(9, 'Samsung', 'Filtrado dos modelos de Samsung Galaxy Note 10', '<p style=\"text-align: justify;\"><strong>Se ha filtrado el dise&ntilde;o del Samsung Galaxy Note 10 y parece ser que habr&aacute; dos modelos de este tel&eacute;fono.</strong><br /><br />Pues no ser&iacute;a la primera vez que se filtra algo asi del Samsung Galaxy Note 10, pues hace algunos d&iacute;as se habia filtrado lo que es la parte trasera del dispositivo, mostrando asi el sistema de c&aacute;maras, seg&uacute;n algunas fuentes, pues tendremos en el Note 10 un sistema de 3 c&aacute;maras y adem&aacute;s un sensor Tof 3D.<br /><br />Este sensor va a mejorar los retratos, ya que es un tipo de sensor que se ha puesto bastante de moda, aunque cabe mencionar que necesita una gran velocidad de tratamiento de datos por parte del procesador, mejora exageramente el recorte y la profundidad de la fotograf&iacute;a. El modelo b&aacute;sico tendr&aacute; la misma configuraci&oacute;n del Galaxy S10 Plus.</p>\r\n<p style=\"text-align: justify;\"><img src=\"/photos/1/galaxy-note-10.jpeg\" alt=\"Galaxy-Note-10\" /></p>\r\n<p style=\"text-align: justify;\">Los nuevos dise&ntilde;os del Galaxy Note 10 nos dejan ver la parte frontal. Como lo mencionabamos al inicio, habr&aacute; dos modelos del Galaxy Note 10. La versi&oacute;n Plus tendr&aacute; una pantalla de 6,75\" pulgadas y el Note 10 un panel de 6,3\" pulgadas, brindando as&iacute; una opci&oacute;n m&aacute;s grande a quienes requieran a&uacute;n de m&aacute;s tama&ntilde;o para trabajar y tener aplicaciones multitarea. Adem&aacute;s del tama&ntilde;o, cambiar&aacute; notoriamente el dise&ntilde;o. En la parte superior de la parte frontal tendremos la abertura que alberga la camar&aacute; frontal. Es algo que se ha podido ver en otros tel&eacute;fonos m&oacute;viles, como los mismos Galaxy S10, pero nunca en la parte central.<br /><br />Todo apunta a que Samsung no estrenar&aacute; la tecnolog&iacute;a de c&aacute;mara frontal bajo la pantalla que ya hemos visto en m&oacute;viles Xiaomi y OPPO.<br /><br />Seg&uacute;n estos dise&ntilde;os, la pantalla estar&aacute; estirada al m&aacute;ximo, con unos bordes laterales inexistentes y unos marcos superior e inferior con dimensiones muy reducidas. Tambi&eacute;n habr&aacute; espacio para el S-Pen. Lo unico malo es que no tendremos puerto Jack de 3,5 mm para auriculares.&nbsp;<br /><br />Bueno en conclusi&oacute;n, toca esperar a ver si estos dise&ntilde;os se acercan al modelo que presente Samsung pr&oacute;ximamente.</p>', 'Gerson Alvarez', 'Samsung.webp', 'samsung', '2019-06-14 18:55:43', '2019-07-15 20:03:15'),
(11, 'Sitios Web', '¿Por qué es importante que una empresa cuente con un sitio web en la actualidad?', '<p style=\"text-align: justify;\"><strong>Muchas empresas hoy en d&iacute;a tienen temor de incursionar en lo que es el mundo del internet, por lo que algunas empresas suelen decir casi siempre: &iquest;Para qu&eacute; necesitan un sitio web, si as&iacute; les va bien?</strong><br /><br />No hay que cerrarse en ese c&iacute;rculo, de que si, as&iacute; como est&aacute;n les va bien ah&iacute; se quedaran, hay que pensar en grande y no temer al cambio, hoy en d&iacute;a, &iquest;as&iacute; como est&aacute; la tecnolog&iacute;a quien no tiene acceso a una computadora o un tel&eacute;fono m&oacute;vil?<br /><br />La mayor parte de las personas en la actualidad pasan casi las 24 horas del d&iacute;a a no m&aacute;s de 100 metros de una computadora o dispositivo m&oacute;vil. &iquest;Entonces por qu&eacute; no aprovechar este medio y llegar a nuestros clientes por medio de la tecnolog&iacute;a?<br /><br />&iquest;Mientras t&uacute; est&aacute;s con la duda de que si tu empresa necesita un sitio web? &nbsp;hay otras que ya est&aacute;n abarcando mercados potenciales.<br /><br />Las personas hoy en d&iacute;a est&aacute;n tomando la costumbre de usar los motores de busqueda para encontrar el producto o servicios que ellos necesitan antes de adquirirlo, esto ahorra mucho tiempo a las personas al momento de visitar un negocio. Entonces para una empresa, lo mejor es estar donde est&aacute;n los usuarios y estos, en su gran mayor&iacute;a, est&aacute;n en l&iacute;nea. Este aumento en el uso de Internet presenta potencial para todos los tipos de empresas siempre que puedan encontrar el momento en que su p&uacute;blico est&aacute; en l&iacute;nea. Contar con un sitio web ya no se considera un lujo si no una necesidad que esta relacionada de manera positiva con el incremento de las ventas, la productividad y el valor del mercado de las empresas.<br /><br /><strong>A continuaci&oacute;n, te presentamos las ventajas de contar con un sitio web:</strong></p>\r\n<blockquote>\r\n<p><strong>1.&nbsp;Alcance mundial de sus productos y servicios:</strong>&nbsp;A trav&eacute;s del sitio Web Usted podr&aacute; llegar a clientes potenciales del mundo entero.</p>\r\n</blockquote>\r\n<blockquote>\r\n<p><strong>2.&nbsp;Competir al lado de los m&aacute;s grandes:</strong>&nbsp;Su imagen en Internet podr&aacute; ser comparada con cualquier gran compa&ntilde;&iacute;a de su mismo sector. Es un terreno imparcial donde el sitio web de una empresa peque&ntilde;a puede ser un tanto o m&aacute;s atractivo que el de una empresa muy grande.</p>\r\n</blockquote>\r\n<blockquote>\r\n<p><strong>3.&nbsp;Disponible las 24 horas, todos los d&iacute;as del a&ntilde;o:</strong> tener presencia en internet es como tener abierta las oficinas&nbsp; las 24 horas, los 7 d&iacute;as de la semana, los 365 d&iacute;as del a&ntilde;o, lo que permite que a&uacute;n fuera del horario de trabajo, la empresa siga produciendo y creciendo.</p>\r\n</blockquote>\r\n<blockquote>\r\n<p><strong>4. Menos Costos Operativos:&nbsp;</strong>Ahorro en gastos de publicidad, reducci&oacute;n de tel&eacute;fono y personal, de modo que cualquier informaci&oacute;n que pudiera necesitar el cliente la podr&aacute; consultar en su p&aacute;gina Web, esto le ahorra tiempo, dinero y reduce el n&uacute;mero de llamadas telef&oacute;nicas de su negocio, locales y de larga distancia.</p>\r\n</blockquote>\r\n<blockquote>\r\n<p><strong>5. Imagen Profesional de su empresa:&nbsp;</strong>Actualmente las empresas respetadas en el medio tienen presencia en la Web. El no contar con un sitio web, puede dar una imagen poco seria y profesional.</p>\r\n</blockquote>', 'Gerson Alvarez', 'Sitios Web.webp', 'sitios-web', '2019-06-14 22:23:20', '2019-07-15 20:03:45');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `telephone` int(12) DEFAULT NULL,
  `email` text CHARACTER SET utf8 COLLATE utf8_bin,
  `message` text CHARACTER SET utf8 COLLATE utf8_bin,
  `company` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `social` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contact_us`
--

INSERT INTO `contact_us` (`id`, `name`, `telephone`, `email`, `message`, `company`, `social`, `created_at`, `updated_at`) VALUES
(17, NULL, NULL, NULL, NULL, '', '', '2019-03-23 02:03:01', '2019-03-23 02:03:01'),
(18, NULL, NULL, NULL, NULL, '', '', '2019-03-23 02:03:15', '2019-03-23 02:03:15'),
(19, 'asda', 1232131, 'atejada@fdevelopershn.com', 'saxa', '', '', '2019-03-23 02:04:19', '2019-03-23 02:04:19'),
(20, 'asda', 1232131, 'atejada@fdevelopershn.com', 'saxa', '', '', '2019-03-23 02:04:58', '2019-03-23 02:04:58'),
(21, 'Prueba', 788121, 'atejada@fdevelopershn.com', 'sadasda', '', '', '2019-03-23 02:07:50', '2019-03-23 02:07:50'),
(22, 'gero', 76343, 'galvarez@fdevelopershn.com', 'ninguna', '', '', '2019-03-23 02:10:31', '2019-03-23 02:10:31'),
(23, 'prueba5', 543232, 'atejada@fdevelopershn.com', 'ni idea tengo', '', '', '2019-03-23 02:12:52', '2019-03-23 02:12:52'),
(24, 'rex', 1231, 'atejada@fdevelopershn.com', 'cas', '', '', '2019-03-23 02:15:49', '2019-03-23 02:15:49'),
(25, 'asda', 3123, 'prueba@fdevelopershn.com', 'ssada', 'qad', 'asda', '2019-03-23 02:29:06', '2019-03-23 02:29:06'),
(26, 'Alejandro', 8990121, 'atejada@fdevelopershn.com', 'ninguna', 'free', 'fb', '2019-03-23 02:32:46', '2019-03-23 02:32:46'),
(27, 'Jose Alejandro', 89781213, 'prueba@fdevelopershn.com', 'aasa', 'freee', 'aaa', '2019-03-23 02:38:13', '2019-03-23 02:38:13'),
(28, 'Gmail', 7845323, 'rlopez@fdevelopershn.com', 'asda', 'freelance', 'asas', '2019-03-23 02:41:30', '2019-03-23 02:41:30'),
(29, 'Alejandro', 433241, 'atejada@fdevelopershn.com', 'ninguna', 'Freelance', 'sii', '2019-03-23 02:48:19', '2019-03-23 02:48:19'),
(30, 'Prueba', 8982131, 'prueba@fdevelopershn.com', 'Una pagina web', 'Freelance Developers', 'Facebook', '2019-03-23 02:55:26', '2019-03-23 02:55:26'),
(31, 'Prueba', 94607821, 'info@fdevelopershn.com', 'Una Aplicacion Movil', 'Freelance Developers', 'Twitter', '2019-03-23 02:56:52', '2019-03-23 02:56:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detail_sliders`
--

CREATE TABLE `detail_sliders` (
  `id` int(11) NOT NULL,
  `id_slider` int(11) NOT NULL,
  `titulo_slider` varchar(55) DEFAULT NULL,
  `description_slider` varchar(120) DEFAULT NULL,
  `text_position` varchar(55) DEFAULT NULL,
  `alt_image` text,
  `link_slider` text,
  `image_slider` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detail_sliders`
--

INSERT INTO `detail_sliders` (`id`, `id_slider`, `titulo_slider`, `description_slider`, `text_position`, `alt_image`, `link_slider`, `image_slider`, `created_at`, `updated_at`) VALUES
(2, 1, 'Desarrollo de páginas web', 'Desarrollo web 100% a la medida', 'center', 'desarrollo paginas web a la medida', NULL, 'Desarrollo de páginas web.webp', '2019-03-22 02:06:56', '2019-07-12 21:27:43'),
(3, 1, 'Desarrollo de sistemas web', 'Diseñamos y desarrollamos sistemas web a medida', 'left', 'desarrollo de sistemas web a la medida', NULL, 'Desarrollo de sistemas web.webp', '2019-03-22 02:08:28', '2019-07-12 21:28:09'),
(4, 1, 'Desarrollo de Apps', 'Desarrollo de Apps para iOS y android', 'right', 'desarrollo de aplicaciones moviles iOS y Android', NULL, 'Desarrollo de Apps.webp', '2019-03-22 02:09:16', '2019-07-12 21:28:25'),
(8, 1, 'E-commerce a medida', 'Desarrollamos lo que tu negocio necesita', 'right', 'Desarrollo de e-commerce, comercio electronico', NULL, 'E-commerce a medida.webp', '2019-05-10 20:28:07', '2019-07-12 21:28:40'),
(9, 1, 'Marketing digital', 'Da a conocer tu empresa al mundo', 'left', 'marketing digital creacion de marcas manejo de redes sociales', NULL, 'Marketing digital.webp', '2019-05-10 20:29:33', '2019-07-12 21:28:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('gergeo-18@hotmail.com', '$2y$10$nHM2A7YUFkOVtwKIGlpReO4IMuzH0PYLRKTki1Bi11EL7zmJyBm9q', '2018-10-27 05:39:17'),
('joseatg95@gmail.com', '$2y$10$r0OgheoLU7DHJAhnOcvQAumlmLVOA7DTGXcgzb3LkBflr869XnSe2', '2018-10-27 05:53:58');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `name_project` varchar(50) NOT NULL,
  `content_project` text NOT NULL,
  `link_project` text,
  `alt_image` text,
  `image_project` text NOT NULL,
  `id_service` int(11) NOT NULL,
  `slug` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `projects`
--

INSERT INTO `projects` (`id`, `name_project`, `content_project`, `link_project`, `alt_image`, `image_project`, `id_service`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Freelance Developers', 'Nuestro sitio web fué diseñado y dessarrollado con Materialize + Javascript + Laravel y Mysql.', NULL, 'computadora mac que muestra el sitio web de freelance', 'Freelance Developers.webp', 1, 'freelance-developers', '2018-10-26 06:05:41', '2019-07-15 19:09:56'),
(2, 'UNICAH', 'Desarrollo Back-End del sitio web de la Universidad Católica de Honduras, utilizando Laravel + Mysql.', 'https://spsp.unicah.edu/', 'Computadora-Mac-web-Unicah', 'UNICAH.webp', 1, 'unicah', '2019-03-22 04:17:10', '2019-07-15 19:10:16'),
(3, 'UNICAH Front-end', 'Maquetación web en Html5 + Css3 y Javascript.', NULL, 'maquetacion-web-unicah', 'UNICAH Front-end.webp', 2, 'unicah-front-end', '2019-03-22 04:18:44', '2019-07-15 19:10:33'),
(4, 'UNICAH APP', 'Aplicación móvil hibrida desarrollada con Ionic + Typescript + Angular y Css.', NULL, 'app-movil-unicah', 'UNICAH APP.webp', 4, 'unicah-app', '2019-03-22 04:24:49', '2019-07-15 19:10:53'),
(5, 'Coming Soon Page', 'Página de lazamiento web, para SYC Marketing Digital desarrollado con Bootstrap.', 'https://sycmarketingdigital.com/', NULL, 'Coming Soon Page.webp', 1, 'coming-soon-page', '2019-06-19 17:24:26', '2019-07-15 19:11:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `name_service` varchar(50) NOT NULL,
  `description_service` varchar(255) NOT NULL,
  `alt_image` text,
  `image_service` text,
  `slug` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `services`
--

INSERT INTO `services` (`id`, `name_service`, `description_service`, `alt_image`, `image_service`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Desarrollo Web', 'Frontend y Backend', 'Icono paginas web', 'Desarrollo Web.webp', 'desarrollo-web', '2018-10-24 08:20:25', '2019-07-12 22:32:53'),
(2, 'Maquetación Web', 'Frameworks CSS', 'Icono maquetacion web', 'Maquetación Web.webp', 'maquetacion-web', '2018-10-24 09:07:59', '2019-07-12 22:34:19'),
(3, 'Aplicaciones Web', 'Desarrollo web', 'Icono aplicaciones web', 'Aplicaciones Web.webp', 'aplicaciones-web', '2019-03-22 03:02:14', '2019-07-12 22:37:30'),
(4, 'Apps Móviles', 'Desarrollo móvil', 'Icono desarrollo de aplicaciones móviles', 'Apps Móviles.webp', 'apps-moviles', '2019-03-22 03:02:49', '2019-07-12 22:37:44');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sliders`
--

CREATE TABLE `sliders` (
  `id` int(11) NOT NULL,
  `name_slider` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sliders`
--

INSERT INTO `sliders` (`id`, `name_slider`, `created_at`, `updated_at`) VALUES
(1, 'Main', '2018-10-25 09:47:51', '2018-10-25 09:47:51'),
(2, 'Portfolio', '2018-10-25 10:18:30', '2018-10-25 10:18:30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subscribe`
--

CREATE TABLE `subscribe` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_user` int(11) DEFAULT NULL,
  `description_user` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_user` text COLLATE utf8_unicode_ci,
  `image_user` text COLLATE utf8_unicode_ci,
  `image_background` text COLLATE utf8_unicode_ci,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` text COLLATE utf8_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `api_token` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `type_user`, `description_user`, `content_user`, `image_user`, `image_background`, `email`, `email_verified_at`, `password`, `slug`, `remember_token`, `api_token`, `created_at`, `updated_at`) VALUES
(1, 'Gerson Alvarez', 1, NULL, NULL, '5c953f7910b07-Gerson.png', '5c953ffc8e930-Gerson.jpg', 'galvarez@fdevelopershn.com', NULL, '$2y$10$.xDiGLVFaR/dWliYvxwYtO7iUmweJi1Asm7.ue8GebotKStLTEiVS', 'gerson-alvarez', 'j7CNgOTeiSe9Y7sniiJtg5a4uYAgLNpwSxgOTp4S0YKBnvqYtICxFoNyng4b', 123, '2018-10-26 06:40:23', '2019-06-12 20:40:46'),
(6, 'Alejandro', 1, NULL, NULL, '5bd6aa33017be-hola.png', '5c953fea8e55f-Alejandro.png', 'atejada@fdevelopershn.com', NULL, '$2y$10$6Sz8lmLS2Y.wuSq1cFWdiutXZT2RYioiM0MP/h2Jy1Mhc8RTm1TyO', 'alejandro', NULL, 0, '2018-10-28 11:23:41', '2019-03-22 20:04:58'),
(7, 'Roberto', 2, 'Back-end Developer', 'Ingeniero en Sistemas', NULL, NULL, 'rlopez@fdevelopershn.com', NULL, '$2y$10$jVB7WAFhd5nzwQet/c9Z6uFAlHfE6sg99E2FtKVoabjjzeCp3hvPW', 'roberto', NULL, NULL, '2019-03-22 19:35:21', '2019-03-22 19:35:21'),
(8, 'jonathan', 1, NULL, NULL, '5c953fbd2dc6c-jonathan.png', NULL, 'jfranco@fdevelopershn.com', NULL, '$2y$10$NA04PPsCf0wOprE4mjKFVOMHzJpYIZXrBoP7I/fDI9Bj0l4MwINmu', 'jonathan', NULL, NULL, '2019-03-22 20:04:13', '2019-03-22 20:04:13');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `detail_sliders`
--
ALTER TABLE `detail_sliders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_slider` (`id_slider`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_service` (`id_service`);

--
-- Indices de la tabla `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `subscribe`
--
ALTER TABLE `subscribe`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de la tabla `detail_sliders`
--
ALTER TABLE `detail_sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `subscribe`
--
ALTER TABLE `subscribe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `detail_sliders`
--
ALTER TABLE `detail_sliders`
  ADD CONSTRAINT `detail_sliders_ibfk_1` FOREIGN KEY (`id_slider`) REFERENCES `sliders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `projects_ibfk_1` FOREIGN KEY (`id_service`) REFERENCES `services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
