<?php

//Views
Route::get('/', 'HomeController@ShowHome');

Route::view('/nosotros','about');

Route::get('/servicios','ServiceController@ShowViewServices');

Route::get('/portafolio','ProjectController@ShowPortfolio');

Route::get('/blog','BlogController@ShowViewPosts');

Route::get('/blog/{slug}','BlogController@ShowViewPost');

Route::get('/contactanos','ContactUSController@contactUS');
Route::post('/contactanos/store','ContactUSController@contactUSPost');

//Views Manager

//Route::get('/', 'UserController@ShowSubscribe');

//Route::post('/fwasdevelopers/admin-manager/subscribe/store','UserController@StoreSubscribe');

Route::group(['middleware' => ['auth']], function() {
Route::get('/fwasdevelopers/admin-manager','UserController@AdminPanel');
Route::get('/fwasdevelopers/admin-manager/manager-projects/app-mobile','UserController@AdminAppMobile');

Route::get('/fwasdevelopers/admin-manager/services','ServiceController@ShowServices');
Route::get('/fwasdevelopers/admin-manager/services/create','ServiceController@CreateService');
Route::post('/fwasdevelopers/admin-manager/services/store','ServiceController@StoreService');
Route::get('/fwasdevelopers/admin-manager/services/edit/{id}','ServiceController@EditService');
Route::post('/fwasdevelopers/admin-manager/services/update/{id}','ServiceController@UpdateService');
Route::get('/fwasdevelopers/admin-manager/services/delete/{id}','ServiceController@DestroyService');

Route::get('/fwasdevelopers/admin-manager/users','UserController@ShowUsers');
Route::get('/fwasdevelopers/admin-manager/users/create','UserController@CreateUser');
Route::post('/fwasdevelopers/admin-manager/users/store','UserController@StoreUser');
Route::get('/fwasdevelopers/admin-manager/users/edit/{id}','UserController@EditUser');
Route::post('/fwasdevelopers/admin-manager/users/update/{id}','UserController@UpdateUser');
Route::get('/fwasdevelopers/admin-manager/users/delete/{id}','UserController@DestroyUser');

Route::get('/fwasdevelopers/admin-manager/projects','ProjectController@ShowProjects');
Route::get('/fwasdevelopers/admin-manager/projects/create','ProjectController@CreateProject');
Route::post('/fwasdevelopers/admin-manager/projects/store','ProjectController@StoreProject');
Route::get('/fwasdevelopers/admin-manager/projects/edit/{id}','ProjectController@EditProject');
Route::post('/fwasdevelopers/admin-manager/projects/update/{id}','ProjectController@UpdateProject');
Route::get('/fwasdevelopers/admin-manager/projects/delete/{id}','ProjectController@DestroyProject');

Route::get('/fwasdevelopers/admin-manager/posts','BlogController@ShowPosts');
Route::get('/fwasdevelopers/admin-manager/posts/create','BlogController@CreatePost');
Route::post('/fwasdevelopers/admin-manager/posts/store','BlogController@StorePost');
Route::get('/fwasdevelopers/admin-manager/posts/edit/{id}','BlogController@EditPost');
Route::post('/fwasdevelopers/admin-manager/posts/update/{id}','BlogController@UpdatePost');
Route::get('/fwasdevelopers/admin-manager/posts/delete/{id}','BlogController@DestroyPost');


Route::get('/fwasdevelopers/admin-manager/sliders','SliderController@ShowSliders');
Route::get('/fwasdevelopers/admin-manager/sliders/create','SliderController@CreateSlider');
Route::post('/fwasdevelopers/admin-manager/sliders/store','SliderController@StoreSlider');
Route::get('/fwasdevelopers/admin-manager/sliders/edit/{id}','SliderController@EditSlider');
Route::post('/fwasdevelopers/admin-manager/sliders/update/{id}','SliderController@UpdateSlider');
Route::get('/fwasdevelopers/admin-manager/sliders/delete/{id}','SliderController@DestroySlider');

Route::get('/fwasdevelopers/admin-manager/detail-sliders','DetailSliderController@ShowDetailSliders');
Route::get('/fwasdevelopers/admin-manager/detail-sliders/create','DetailSliderController@CreateDetailSlider');
Route::post('/fwasdevelopers/admin-manager/detail-sliders/store','DetailSliderController@StoreDetailSlider');
Route::get('/fwasdevelopers/admin-manager/detail-sliders/edit/{id}','DetailSliderController@EditDetailSlider');
Route::post('/fwasdevelopers/admin-manager/detail-sliders/update/{id}','DetailSliderController@UpdateDetailSlider');
Route::get('/fwasdevelopers/admin-manager/detail-sliders/delete/{id}','DetailSliderController@DestroyDetailSlider');

//Routes Manager-Projects

//Routes Apps Mobile

//Convert Currency
Route::get('/fwasdevelopers/admin-manager/manager-projects/app-mobile/convert-currency','CurrencyController@ShowCoins');
Route::get('/fwasdevelopers/admin-manager/manager-projects/app-mobile/convert-currency/create','CurrencyController@CreateCoin');
Route::post('/fwasdevelopers/admin-manager/manager-projects/app-mobile/convert-currency/store','CurrencyController@StoreCoin');
Route::get('/fwasdevelopers/admin-manager/manager-projects/app-mobile/convert-currency/edit/{id}','CurrencyController@EditCoin');
Route::post('/fwasdevelopers/admin-manager/manager-projects/app-mobile/convert-currency/update/{id}','CurrencyController@UpdateCoin');
Route::get('/fwasdevelopers/admin-manager/manager-projects/app-mobile/convert-currency/delete/{id}','CurrencyController@DestroyCoin');


});

Route::get('/fwdadmin/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/fwdadmin/login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');


