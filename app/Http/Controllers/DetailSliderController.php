<?php

namespace freelance_web\Http\Controllers;

use Illuminate\Http\Request;
use freelance_web\Models\DetailSlider;
use freelance_web\Models\Slider;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

class DetailSliderController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function ShowDetailSliders(Request $request)
  {
    $detail_sliders = DetailSlider::Name($request->get('name'))->SimplePaginate(10);
    $sliders = Slider::all();

    return view('manager.detail-sliders.show_detail_sliders',
    compact('detail_sliders','sliders'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function CreateDetailSlider()
  {
    $sliders = Slider::all();
    return view('manager.detail-sliders.create_detail_slider',compact('sliders'));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function StoreDetailSlider(Request $request)
  {
    $detail_slider = new DetailSlider;

    $this->validate($request,[
      'image_slider' => 'required',
    ]);

    $detail_slider->id_slider= $request->input('slider_id');
    $slider = Slider::where('id',$detail_slider->id_slider)->first();

    $detail_slider->titulo_slider= $request->input('titulo_slider');
    $detail_slider->description_slider= $request->input('description_slider');
    $detail_slider->text_position= $request->input('text_position');
    $detail_slider->alt_image= $request->input('alt_image');
    $detail_slider->link_slider= $request->input('link_slider');
    $file_detail_slider = $request->file('image_slider');
    $destinationPath= 'img/sliders';
    $image_slider_extension = $request->file('image_slider')->getClientOriginalExtension();
    $image = $detail_slider->titulo_slider.".".$image_slider_extension;
    $detail_slider->image_slider = $image;
    $file_detail_slider->move($destinationPath, $image);
    $true = 1;

    $detail_slider->save();
    return redirect(action('DetailSliderController@ShowDetailSliders'))
    ->with('edited',$true)->with('useredited','The Detail Slider was Created in "'.$slider->name_slider.'"');
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function EditDetailSlider($id)
  {
    $sliders = Slider::all();
    $detail_slider = DetailSlider::find($id);
    $sliderunique = Slider::where('id',$detail_slider->id_slider)->first();
    return view('manager.detail-sliders.edit_detail_slider',
    compact('sliders','detail_slider','sliderunique'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function UpdateDetailSlider(Request $request, $id)
  {
    $detail_slider = DetailSlider::find($id);

    $detail_slider->id_slider= $request->input('slider_id');
    $slider = Slider::where('id',$detail_slider->id_slider)->first();

    $detail_slider->titulo_slider= $request->input('titulo_slider');
    $detail_slider->description_slider= $request->input('description_slider');
    $detail_slider->text_position= $request->input('text_position');
    $detail_slider->alt_image= $request->input('alt_image');
    $detail_slider->link_slider= $request->input('link_slider');
    $file_detail_slider = $request->file('image_slider');
    if (  $file_detail_slider == "") {
      // code...
    }
    else
    {
      File::Delete('img/sliders/'.$detail_slider->image_slider);
      $destinationPath= 'img/sliders';
      $image_slider_extension = $request->file('image_slider')->getClientOriginalExtension();
      $image = $detail_slider->titulo_slider.".".$image_slider_extension;
      $detail_slider->image_slider = $image;
      $file_detail_slider->move($destinationPath, $image);
    }
    $true = 1;
    $detail_slider->save();
    return redirect(action('DetailSliderController@ShowDetailSliders'))
    ->with('edited',$true)->with('useredited','The Detail Slider was Edited in "'.$slider->name_slider.'"');

  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function DestroyDetailSlider($id)
  {
    $detail_slider = DetailSlider::find($id);
    $slider = Slider::where('id',$detail_slider->id_slider)->first();
    $detail_slider->delete();
    $true=1;
    File::Delete('img/sliders/'.$detail_slider->image_slider);
    return redirect(action('DetailSliderController@ShowDetailSliders'))
    ->with('edited',$true)->with('useredited','The Detail Slider was Destroyed in "'.$slider->name_slider.'"');
  }
}
