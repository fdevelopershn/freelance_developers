<?php

namespace freelance_web\Http\Controllers;

use Illuminate\Http\Request;
use freelance_web\Models\DetailSlider;
use freelance_web\Models\Service;
use freelance_web\Models\Project;
use freelance_web\Models\Blog;
use Jenssegers\Date\Date;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
   /* public function __construct()
    {
        $this->middleware('auth');
    }
*/
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function ShowHome()
    {
        $detail_sliders = DetailSlider::all();
        $services = Service::take(4)->get();
        $projects = Project::take(3)->get();
        $posts = Blog::orderby("created_at","DESC")->take(3)->get();

        return view('home',compact('detail_sliders','services','projects','posts'));
    }
}