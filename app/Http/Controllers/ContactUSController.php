<?php

namespace freelance_web\Http\Controllers;

use Illuminate\Http\Request;
use freelance_web\Http\Requests;
use freelance_web\Models\ContactUS;
use Mail;

class ContactUSController extends Controller
{
  public function contactUS()
 {
     return view('contact');
 }

 public function contactUSPost(Request $request)
  {
      $contactus = new ContactUS;

      $this->validate($request,[
        'name' => 'required',
        'telephone' => 'required',
        'email' => 'required',
        'message' => 'required',
        'company' => 'required',
        'social' => 'required',
        'g-recaptcha-response' => 'required|recaptchav3:contacto,0.5',
      ]);

      $contactus->name = $request->input('name');
      $contactus->telephone = $request->input('telephone');
      $contactus->email = $request->input('email');
      $contactus->message = $request->input('message');
      $contactus->company= $request->input('company');
      $contactus->social = $request->input('social');

      $contactus->save();

      Mail::send('email',
       array(
           'name' => $request->get('name'),
           'telephone' => $request->get('telephone'),
           'email' => $request->get('email'),
           'company' => $request->get('company'),
           'social' => $request->get('social'),
           'user_message' => $request->get('message')
       ), function($message)
   {
       $message->from('fdevelopershn@gmail.com');
       $message->to('fdevelopershn@gmail.com', 'Admin')->subject('Contacts');
   });

    return redirect(action('ContactUSController@contactUS'))
    ->with('success','Gracias por contactarnos!');

  }

}
