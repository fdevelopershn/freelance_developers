<?php

namespace freelance_web\Http\Controllers;

use Illuminate\Http\Request;
use freelance_web\Models\Service;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

class ServiceController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  
  /*View Services*/
  public function ShowViewServices(Request $request)
  {
    $services = Service::all();

    return view('services',compact('services'));
  }


  public function ShowServices(Request $request)
  {
    $services = Service::Name($request->get('name'))->SimplePaginate(10);

    return view('manager.services.show_services',compact('services'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function CreateService()
  {
    return view('manager.services.create_service');
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function StoreService(Request $request)
  {
    $service = new Service;

    $this->validate($request,[
      'name_service' => 'required',
      'image_service' => 'required',
    ]);

    $service->name_service= $request->input('name_service');
    $service->description_service= $request->input('description_service');
    $service->alt_image= $request->input('alt_image');
    $file_service = $request->file('image_service');
    $destinationPath= 'img/services';
    $image_service_extension = $request->file('image_service')->getClientOriginalExtension();
    $image = $service->name_service.".".$image_service_extension;
    $service->image_service = $image;
    $file_service->move($destinationPath, $image);
    $true = 1;

    $service->save();
    return redirect(action('ServiceController@ShowServices'))
    ->with('edited',$true)->with('useredited','The Service "'.$service->name_service.'" was Created');


  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function EditService($id)
  {
    $service = Service::find($id);

    return view('manager.services.edit_service',compact('service'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function UpdateService(Request $request, $id)
  {
    $service = Service::find($id);

    $this->validate($request,[
      'name_service' => 'required',
    ]);

    $service->name_service= $request->input('name_service');
    $service->description_service= $request->input('description_service');
    $service->alt_image= $request->input('alt_image');
    $file_service = $request->file('image_service');
    if ($file_service == "") {
      // code...
    }else {
      File::Delete('img/services/'.$service->image_service);
      $destinationPath= 'img/services';
      $image_service_extension = $request->file('image_service')->getClientOriginalExtension();
      $image = $service->name_service.".".$image_service_extension;
      $service->image_service = $image;
      $file_service->move($destinationPath, $image);
    }
    $true = 1;
    $service->save();
    return redirect(action('ServiceController@ShowServices'))
    ->with('edited',$true)->with('useredited','The Service "'.$service->name_service.'" was Edited');

  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function DestroyService($id)
  {
    $service = Service::find($id);
    $true = 1;
    $service->delete();
    File::Delete('img/services/'.$service->image_service);
    return redirect(action('ServiceController@ShowServices'))
    ->with('edited',$true)->with('useredited','The Service "'.$service->name_service.'" was Destroyed');

  }
}