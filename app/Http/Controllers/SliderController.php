<?php

namespace freelance_web\Http\Controllers;

use Illuminate\Http\Request;
use freelance_web\Models\Slider;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

class SliderController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function ShowSliders(Request $request)
  {
    $sliders = Slider::Name($request->get('name'))->SimplePaginate(10);

    return view('manager.sliders.show_sliders',compact('sliders'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function CreateSlider()
  {
    return view('manager.sliders.create_slider');
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function StoreSlider(Request $request)
  {
    $slider = new Slider;

    $this->validate($request,[
      'name_slider' => 'required',
    ]);

    $slider->name_slider= $request->input('name_slider');
    $true=1;

    $slider->save();
    return redirect(action('SliderController@ShowSliders'))
    ->with('edited',$true)->with('useredited','The Slider "'.$slider->name_slider.'" was Created');
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function EditSlider($id)
  {
    $slider = Slider::find($id);

    return view('manager.sliders.edit_slider',compact('slider'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function UpdateSlider(Request $request, $id)
  {
    $slider = Slider::find($id);

    $this->validate($request,[
      'name_slider' => 'required',
    ]);

    $slider->name_slider= $request->input('name_slider');
    $true = 1;
    $slider->save();
    return redirect(action('SliderController@ShowSliders'))
    ->with('edited',$true)->with('useredited','The Slider "'.$slider->name_slider.'" was Edited');

  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function DestroySlider($id)
  {
    $slider = Slider::find($id);
    $true = 1;
    $slider->delete();
    return redirect(action('SliderController@ShowSliders'))
    ->with('edited',$true)
    ->with('useredited','The Slider "'.$slider->name_slider.'" was Destroyed');
  }
}
