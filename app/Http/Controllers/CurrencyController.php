<?php

namespace freelance_web\Http\Controllers;

use Illuminate\Http\Request;
use freelance_web\Models\api_convert_coins\Coin;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

class CurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ShowCoins(Request $request)
    {
    $coins = Coin::Name($request->get('name'))
            ->orderby('name_coin','ASC')
            ->SimplePaginate(10);

    $datecoin = Coin::orderby('updated_at','ASC')->first();

    return view('manager-projects.convert-currency.show_coins',
           compact('coins','datecoin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function CreateCoin()
    {
        return view('manager-projects.convert-currency.create_coin');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function StoreCoin(Request $request)
    {
        $coin = new Coin;

    $this->validate($request,[
      'name_coin' => 'required',
      'sign_coin' => 'required',
      'value_coin' => 'required',
      'image_coin' => 'required',
    ]);

    $coin->name_coin= $request->input('name_coin');
    $coin->sign_coin= $request->input('sign_coin');
    $coin->value_coin= $request->input('value_coin');
    $file_coin = $request->file('image_coin');
    $destinationPath= 'img\currency\currency_countries';
    $image_coin_extension = $request->file('image_coin')->getClientOriginalExtension();
    $image = uniqid()."-".$coin->name_coin.".".$image_coin_extension;
    $coin->image_coin = $image;
    $file_coin->move($destinationPath, $image);
    $true = 1;
    $coin->save();
    return redirect(action('CurrencyController@ShowCoins'))
    ->with('edited',$true)->with('useredited','The Coin "'.$coin->name_coin.'" was Created');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function EditCoin($id)
    {
        $coin = Coin::find($id);

        return view('manager-projects.convert-currency.edit_coin',compact('coin'));
   

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function UpdateCoin(Request $request, $id)
    {
        $coin = Coin::find($id);

        $this->validate($request,[
          'name_coin' => 'required',
          'sign_coin' => 'required',
          'value_coin' => 'required',
        ]);
    
        $coin->name_coin= $request->input('name_coin');
        $coin->sign_coin= $request->input('sign_coin');
        $coin->value_coin= $request->input('value_coin');
        $file_coin = $request->file('image_coin');

        if ($file_coin == "") {
            // code...
          }else {
        $destinationPath= 'img\currency\currency_countries';
        $image_coin_extension = $request->file('image_coin')->getClientOriginalExtension();
        $image = uniqid()."-".$coin->name_coin.".".$image_coin_extension;
        $coin->image_coin = $image;
        $file_coin->move($destinationPath, $image);
          }

        $true = 1;
        $coin->save();
        return redirect(action('CurrencyController@ShowCoins'))
        ->with('edited',$true)->with('useredited','The Coin "'.$coin->name_coin.'" was Edited');
   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function DestroyCoin($id)
    {
        $coin = Coin::find($id);
        $true = 1;
        $coin->delete();
        File::Delete('img/currency/currency_countries/'.$coin->image_coin);
        return redirect(action('CurrencyController@ShowCoins'))
        ->with('edited',$true)->with('useredited','The Coin "'.$coin->name_coin.'" was Destroyed');
    
    }
}
