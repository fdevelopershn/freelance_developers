<?php

namespace freelance_web\Http\Controllers;

use Illuminate\Http\Request;
use freelance_web\Models\User;
use freelance_web\Models\Subscribe;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;


class UserController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function ShowUsers(Request $request)
  {
    if(Auth::user()->type_user==1)
    {
      $users = User::Name($request->get('name'))->SimplePaginate(10);

      return view('manager.users.show_users',compact('users'));
    }
    else
    {
      return redirect(action('UserController@AdminPanel'));
    }

  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function CreateUser()
  {
    if(Auth::user()->type_user==1)
    {
      return view('manager.users.create_user');
    }
    else
    {
      return redirect(action('UserController@AdminPanel'));
    }
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function StoreUser(Request $request)
  {
    if(Auth::user()->type_user==1)
    {
      $user = new User;

      $this->validate($request,[
        'name' => 'required',
        'email' => 'required|email',
        'password' => 'required|min:6',
      ]);

      $user->name = $request->input('name');
      $user->description_user = $request->input('description_user');
      $user->content_user = $request->input('content_user');
      $user->type_user = $request->input('type_user');
      $user->email = $request->input('email');
      $user->password = bcrypt($request->input('password'));

      if ($request->file('image_user')=="") {

      }
      else
      {
        $file_user = $request->file('image_user');
        $destinationPath= 'img/users';
        $image_user_extension = $request->file('image_user')->getClientOriginalExtension();
        $image = uniqid()."-".$user->name.".".$image_user_extension;
        $user->image_user = $image;
        $file_user->move($destinationPath, $image);
      }

      if ($request->file('image_background')=="") {

      }
      else
      {
        $file_background = $request->file('image_background');
        $destinationPath= 'img/backgrounds';
        $image_background_extension = $request->file('image_background')->getClientOriginalExtension();
        $background = $user->name.".".$image_background_extension;
        $user->image_background = $background;
        $file_background->move($destinationPath, $background);
      }
      $true = 1;
      $user->save();

      return redirect(action('UserController@ShowUsers'))
      ->with('edited',$true)->with('useredited','The user "'.$user->name.'" was Create');
    }
    else
    {
      return redirect(action('UserController@AdminPanel'));
    }

  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function AdminPanel()
  {
    return view('manager.admin-manager');
  }

  public function AdminAppMobile()
  {
    return view('manager-projects.app_mobile');
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function EditUser($id)
  {
    $user = User::find($id);

    if(Auth::user()->type_user==1)
    {
      return view('manager.users.edit_user',compact('user'));
    }
    elseif(Auth::user()->type_user==2)
    {
      $user = User::find(Auth::user()->id);
      return view('manager.users.edit_user',compact('user'));
    }
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function UpdateUser(Request $request, $id)
  {
    $user = User::find($id);

    $this->validate($request,[
      'name' => 'required',
      'email' => 'required|email',
    ]);

    $user->name = $request->input('name');
    $user->description_user = $request->input('description_user');
    $user->content_user = $request->input('content_user');
    $user->email = $request->input('email');

    if ($request->input('type_user')=="") {

    }else {
      $user->type_user = $request->input('type_user');
    }



    if ($request->input('password')=="") {

    }else {
      $this->validate($request,[
        'password' => 'min:6',
      ]);
      $user->password = bcrypt($request->input('password'));
    }

    if ($request->file('image_user')=="") {

    }else {
      File::Delete('img/users/'.$user->image_user);
      $file_user = $request->file('image_user');
      $destinationPath= 'img/users';
      $image_user_extension = $request->file('image_user')->getClientOriginalExtension();
      $image = $user->name.".".$image_user_extension;
      $user->image_user = $image;
      $file_user->move($destinationPath, $image);
    }

    if ($request->file('image_background')=="") {

    }
    else {
      File::Delete('img/backgrounds/'.$user->image_background);
      $file_background = $request->file('image_background');
      $destinationPath= 'img/backgrounds';
      $image_background_extension = $request->file('image_background')->getClientOriginalExtension();
      $background = uniqid()."-".$user->name.".".$image_background_extension;
      $user->image_background = $background;
      $file_background->move($destinationPath, $background);
    }
    $true = 1;
    $user->save();

    if (Auth::user()->type_user==1) {
      return redirect(action('UserController@ShowUsers'))
      ->with('edited',$true)->with('useredited','The user "'.$user->name.'" was Edited');
    }elseif ($user->type_user==1) {
      return redirect(action('UserController@ShowUsers'))
      ->with('edited',$true)->with('useredited','The user "'.$user->name.'" was Edited');
    }elseif ($user->type_user==2) {
      return redirect(action('UserController@AdminPanel'))
      ->with('edited',$true)->with('useredited','The user "'.$user->name.'" was Edited');
    }

  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function DestroyUser($id)
  {
    if(Auth::user()->type_user==1){
      $user = User::find($id);
      $true = 1;
      $user->delete();
      File::Delete('img/users/'.$user->image_user);
      File::Delete('img/backgrounds/'.$user->image_background);
      return redirect(action('UserController@ShowUsers'))
      ->with('edited',$true)->with('useredited','The user "'.$user->name.'" was Destroyed');;
    }
    else
    {
      return redirect(action('UserController@AdminPanel'));
    }
  }


  public function ShowSubscribe()
  {

      return view('comingsoon');
    
  }

  public function StoreSubscribe(Request $request)
  {
    $subscribe = new Subscribe;

   

    $this->validate($request,[
      'email' => 'required|email|unique:subscribe',
      'g-recaptcha-response' => 'required|captcha',
    ]);
    print('done');
    $subscribe->email = $request->input('email');
   
    $subscribe->save();

      return redirect(action('UserController@ShowSubscribe'))
      ->with('action','Gracias por subscribirse');
    
  }

  
}