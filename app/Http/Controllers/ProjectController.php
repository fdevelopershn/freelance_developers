<?php

namespace freelance_web\Http\Controllers;

use Illuminate\Http\Request;
use freelance_web\Models\Project;
use freelance_web\Models\Service;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

class ProjectController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */

  /*Show Views Projects*/
  public function ShowPortfolio(Request $request)
  {
    $projects = Project::all();
    $services = Service::all();

    return view('portfolio',compact('projects','services'));
  }

  /*Show Views Manager Projects*/
  public function ShowProjects(Request $request)
  {

    $projects = Project::Name($request->get('name'))->SimplePaginate(10);
    $services = Service::all();

    return view('manager.projects.show_projects',compact('projects','services'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function CreateProject()
  {
    $services = Service::all();

    return view('manager.projects.create_project',compact('services'));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function StoreProject(Request $request)
  {
    $project = new Project;

    $this->validate($request,[
      'name_project' => 'required',
      'content_project' => 'required',
      'image_project' => 'required',
    ]);

    $project->id_service= $request->input('service_id');
    $service = Service::where('id',$project->id_service)->first();

    $project->name_project= $request->input('name_project');
    $project->content_project= $request->input('content_project');
    $project->link_project= $request->input('link_project');
    $file_project = $request->file('image_project');
    $destinationPath= 'img/projects';
    $image_project_extension = $request->file('image_project')->getClientOriginalExtension();
    $image = $project->name_service.".".$image_project_extension;
    $project->image_project = $image;
    $file_project->move($destinationPath, $image);
    $true = 1;
    $project->save();
    return redirect(action('ProjectController@ShowProjects'))
    ->with('edited',$true)->with('useredited','The Project "'.$project->name_project.'" was Created');

  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function EditProject($id)
  {
    $project = Project::find($id);
    $serviceunique = Service::where("id",$project->id_service)->first();
    $services = Service::all();

    return view('manager.projects.edit_project',compact('services',
    'project','serviceunique'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function UpdateProject(Request $request, $id)
  {
    $project = Project::find($id);

    $this->validate($request,[
      'name_project' => 'required',
      'content_project' => 'required',
    ]);

    $project->id_service= $request->input('service_id');
    $service = Service::where('id',$project->id_service)->first();

    $project->name_project= $request->input('name_project');
    $project->alt_image= $request->input('alt_image');
    $project->content_project= $request->input('content_project');
    $project->link_project= $request->input('link_project');
    $file_project = $request->file('image_project');
    if ($file_project == "") {
      // code...
    }else {
      File::Delete('img/projects/'.$project->image_project);
      $destinationPath= 'img/projects';
      $image_project_extension = $request->file('image_project')->getClientOriginalExtension();
      $image = $project->name_project.".".$image_project_extension;
      $project->image_project = $image;
      $file_project->move($destinationPath, $image);
    }
    $true=1;
    $project->save();
    return redirect(action('ProjectController@ShowProjects'))
    ->with('edited',$true)->with('useredited','The Project "'.$project->name_project.'" was Edited');

  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function DestroyProject($id)
  {
    $project = Project::find($id);
    $true = 1;
    $project->delete();
    File::Delete('img/projects/'.$project->image_project);
    return redirect(action('ProjectController@ShowProjects'))
    ->with('edited',$true)->with('useredited','The Project "'.$project->name_project.'" was Destroyed');

  }
}
