<?php

namespace freelance_web\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use freelance_web\Models\Blog;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Jenssegers\Date\Date;

class BlogController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

      /*Show Views Posts*/
    public function ShowViewPosts(Request $request)
    {
     $posts = Blog::orderby("created_at","DESC")->get();
     Date::setLocale('es');

     return view('blog',compact('posts'));
    }

    public function ShowViewPost($slug)
    {
     $post = Blog::whereSlug($slug)->first();
     Date::setLocale('es');

     return view('blog.newsblog',compact('post'));
    }

     /*Show Views Posts Manager*/
    public function ShowPosts(Request $request)
    {
     $posts = Blog::Name($request->get('name'))->SimplePaginate(10);

     return view('manager.blog.show_posts',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function CreatePost()
    {
     return view('manager.blog.create_post');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function StorePost(Request $request)
    {
        $post = new Blog;

        $this->validate($request,[
          'heading_post' => 'required',
          'title_post' => 'required',
          'content_post' => 'required',
          'image_post' => 'required',
        ]);
    
        $post->heading = $request->input('heading_post');
        $post->title = $request->input('title_post');
        $post->content = $request->input('content_post');
        $post->author = Auth::user()->name;
  
        if ($request->file('image_post')=="") {

        }
        else
        {
        $file_post = $request->file('image_post');
        $destinationPath= 'img/blog';
        $image_post_extension = $request->file('image_post')->getClientOriginalExtension();
        $image = $post->heading.".".$image_post_extension;
        $post->image= $image;
        $file_post->move($destinationPath, $image);
        }

        $true = 1;
        $post->save();

        return redirect(action('BlogController@ShowPosts'))
        ->with('edited',$true)->with('useredited','The Post "'.$post->heading.'" was Created');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function EditPost($id)
    {
     $post = Blog::find($id);

     return view('manager.blog.edit_post',compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function UpdatePost(Request $request, $id)
    {
        $post = Blog::find($id);

        $this->validate($request,[
          'heading_post' => 'required',
          'title_post' => 'required',
          'content_post' => 'required',
        ]);
    
        $post->heading = $request->input('heading_post');
        $post->title = $request->input('title_post');
        $post->content = $request->input('content_post');
  
        if ($request->file('image_post')=="") {

        }
        else
        {
        File::Delete('img/blog/'.$post->image);
        $file_post = $request->file('image_post');
        $destinationPath= 'img/blog';
        $image_post_extension = $request->file('image_post')->getClientOriginalExtension();
        $image = $post->heading.".".$image_post_extension;
        $post->image= $image;
        $file_post->move($destinationPath, $image);
        }

        $true = 1;
        $post->save();
        
        return redirect(action('BlogController@ShowPosts'))
        ->with('edited',$true)->with('useredited','The Post "'.$post->heading.'" was Edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function DestroyPost($id)
    {
        $post= Blog::find($id);
        $true = 1;
        $post->delete();
        File::Delete('img/blog/'.$post->image);
        return redirect(action('BlogController@ShowPosts'))
        ->with('edited',$true)->with('useredited','The user "'.$post->heading.'" was Destroyed');
    }
}