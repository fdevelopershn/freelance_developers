<?php

namespace freelance_web\Models;

use Spatie\Sluggable\Slug;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
  use HasSlug;
    protected $table = 'projects';

    public function getSlugOptions() : SlugOptions
    {
      return SlugOptions::create()
      ->generateSlugsFrom('name_project')
      ->saveSlugsTo('slug');
    }

    public function scopeName($query,$name){
      if (trim($name) == "") {
      }else
      $query->where('name_project',"LIKE","%$name%");
    }

}
