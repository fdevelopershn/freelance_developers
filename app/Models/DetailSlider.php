<?php

namespace freelance_web\Models;

use Illuminate\Database\Eloquent\Model;

class DetailSlider extends Model
{
    protected $table = 'detail_sliders';

    public function scopeName($query,$name){
      if (trim($name) == "") {
      }else
      $query->where('description_slider',"LIKE","%$name%");
    }
}
