<?php

namespace freelance_web\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $table = 'sliders';

    public function scopeName($query,$name){
      if (trim($name) == "") {
      }else
      $query->where('name_slider',"LIKE","%$name%");
    }
}
