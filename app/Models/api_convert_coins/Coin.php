<?php

namespace freelance_web\Models\api_convert_coins;

use Illuminate\Database\Eloquent\Model;

class Coin extends Model
{
    protected $connection = 'api_convert_coins';

    protected $table = 'coins';

    public function scopeName($query,$name){
        if (trim($name) == "") {
        }else
        $query->where('name_coin',"LIKE","%$name%");
      }
}
