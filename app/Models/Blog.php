<?php

namespace freelance_web\Models;

use Spatie\Sluggable\Slug;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Jenssegers\Date\Date;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasSlug;
  
    protected $table = 'blog';

    public function getSlugOptions() : SlugOptions
    {
      return SlugOptions::create()
      ->generateSlugsFrom('heading')
      ->saveSlugsTo('slug');
    }

    public function scopeName($query,$name){
      if (trim($name) == "") {
        }else
        $query->where('heading',"LIKE","%$name%");
    }

    public function getCreatedAtAttribute($date){
      return new Date($date);
    }

}
