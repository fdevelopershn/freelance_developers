<?php

namespace freelance_web\Models;

use Spatie\Sluggable\Slug;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use HasSlug;
    protected $table = 'users';

    public function getSlugOptions() : SlugOptions
    {
      return SlugOptions::create()
      ->generateSlugsFrom('name')
      ->saveSlugsTo('slug');
    }

    public function scopeName($query,$name){
      if (trim($name) == "") {
      }else
      $query->where('name',"LIKE","%$name%");
    }
}
