@extends('layouts.admin-manager')

@section('content')
  <div class="row">
    <h2>Create Coin <span class="blue-text"><</span>F<span class="blue-text">></span></h2>

    <form class="col s12" action="{{ URL('/fwasdevelopers/admin-manager/manager-projects/app-mobile/convert-currency/store') }}"
     method="post" enctype="multipart/form-data">
     {{ csrf_field() }}
      <div class="row">

        <div class="col s12">
          <div class="input-field col s6">
            <input name="name_coin" type="text" class="validate">
            <label for="last_name">Name Coin</label>
          </div>
        </div>

        <div class="col s12">
          <div class="input-field col s6">
            <input name="sign_coin" type="text" class="validate">
            <label for="last_name">Sign Coin</label>
          </div>
        </div>

        <div class="col s12">
          <div class="input-field col s6">
            <input name="value_coin" type="text" class="validate">
            <label for="last_name">Value Coin</label>
          </div>
        </div>

        <div class="file-field input-field ">
          <div class="input-field col s6">
            <div class="btn hoverable">
              <span>File</span>
              <input name="image_coin" type="file">
            </div>
            <div class="file-path-wrapper ">
              <input class="file-path validate" type="text">
            </div>
          </div>
        </div>
      </div>
      <button class="btn green waves-effect waves-light radius-buttons hoverable" type="submit" name="action">Save
   <i class="material-icons right">send</i>
 </button>
 <a href="/fwasdevelopers/admin-manager/manager-projects/app-mobile/convert-currency"
 class="btn waves-effect blue waves-light radius-buttons hoverable" type="submit" name="action">Back
<i class="material-icons right">arrow_back</i>
</a>
    </form>
@endsection