@extends('layouts.admin-manager')

@section('content')
  <div class="row">
    <h2>Coins <span class="blue-text"><</span>F<span class="blue-text">></span></h2>

<div class="col s12 l4">
    <a href="/fwasdevelopers/admin-manager/manager-projects/app-mobile/convert-currency/create"
    class="waves-effect green darken-1 btn-small button-set radius-buttons hoverable">
    <i class="material-icons right">add</i>Add New Coin</a>
</div>
<form enctype="multipart/form-data" action="{{ URL('/fwasdevelopers/admin-manager/manager-projects/app-mobile/convert-currency') }}" method="get" role="form">
  <div class="col s2.5 l2.5 search">
    <a href="/fwasdevelopers/admin-manager/manager-projects/app-mobile/convert-currency" class="waves-effect blue darken-1 btn-small radius-buttons hoverable">
      <i class="material-icons left">autorenew</i>Refresh</a>
  </div>
<div class="col s0.5 l0.5 search">
  <button class="waves-effect blue darken-1 btn-small radius-buttons hoverable" type="submit">
    <i class="material-icons">search</i></button>
</div>
<div class="col s4 l4 search">
 <input name="name" style="margin-top:-2%"  type="text" placeholder="Search" >
</div>
</form>
    <table class="center-tr">
      <thead>
        <tr>
          <th>#</th>
          <th>Name Coin</th>
          <th>Value Coin (Dolar)<br>Last Updated: ({{ $datecoin->updated_at }})</th>
          <th>Image Coin</th>
          <th>Edit</th>
          <th>Delete</th>
        </tr>
      </thead>
      <tbody>

          @foreach ($coins as $key => $coin)
              <tr>
            <td>{{ $key+1}}</td>
            <td>{{ $coin->name_coin }}</td>
            <td>{{ $coin->sign_coin }} {{ $coin->value_coin }}</td>  
            <td> <img class="image-admin" src="/img/currency/currency_countries/{{ $coin->image_coin }}" alt=""> </td>
            <td>
              <a href="/fwasdevelopers/admin-manager/manager-projects/app-mobile/convert-currency/edit/{{ $coin->id }}"
                class="waves-effect blue darken-1 btn-small radius-buttons hoverable">
                <i class="material-icons">colorize</i></a></td>
                  <td>
                <a onClick="return confirm('¿Estas seguro que deseas eliminar esta Publicacion?');"
                href="/fwasdevelopers/admin-manager/manager-projects/app-mobile/convert-currency/delete/{{ $coin->id }}"
                  class="waves-effect red btn-small radius-buttons hoverable"><i class="material-icons">delete</i></a>
                </td>
                  </tr>
              @endforeach

          </tbody>
        </table>
{{ $coins->render('manager.pagination_materialize') }}
      </div>
      <a href="/fwasdevelopers/admin-manager/manager-projects/app-mobile"
      class="btn waves-effect blue darken-1 waves-light right radius-buttons hoverable" type="submit" name="action">Back
     <i class="material-icons right">arrow_back</i>
     </a>
     @include('includes.admin-manager._sweetalertaction')
@endsection
