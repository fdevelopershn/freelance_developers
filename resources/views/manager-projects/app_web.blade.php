@extends('layouts.admin-manager')

@section('content')
@include('includes.admin-manager._variablespanel')
  <div class="row">
    <h2>Apps Web <span class="blue-text"><</span>F<span class="blue-text">></span></h2>
    @for ($x = 0; $x < 5; $x++)
    @if ((Auth::user()->type_user==1 && $x>=0) || (Auth::user()->type_user==2 && $x!=0))
    <div class="col s12 m4 center-card-manager">
      <a href="{{ subnavrightrefpanel[0][$x]}}">
        <div class="waves-effect card-panel center-card-manager hoverable">
          <i class="large material-icons ">{{ subnavrighticonspanel[0][$x]}}</i>
          <p>{{ subnavrightpanel[0][$x]}}</p>
          </div>
        </a>
      </div>
      @endif
    @endfor
@endsection
