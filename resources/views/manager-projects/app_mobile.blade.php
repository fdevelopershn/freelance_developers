@extends('layouts.admin-manager')

@section('content')
@include('includes.admin-manager._variablespanel')
  <div class="row">
    <h2>Apps Mobile<span class="blue-text"><</span>F<span class="blue-text">></span></h2>
    @for ($x = 0; $x < 1; $x++)
    <div class="col s12 m4 center-card-manager">
      <a href="{{ appmobileref[$x]}}">
        <div class="waves-effect card-panel center-card-manager hoverable">
          <i class="large material-icons ">{{ appmobileicons[$x]}}</i>
          <p>{{ appmobile[$x]}}</p>
          </div>
        </a>
      </div>
    @endfor
@endsection
