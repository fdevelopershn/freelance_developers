@extends('layouts.freelance')

@section('title', 'Servicios de diseño y desarrollo de sitios web modernos')
@section('urlcanonical','https://fdevelopershn.com/servicios')
@section('description', 'Ofrecemos una gama completa de servicios de desarrollo web y móvil. Nuestro objetivo es proporcionar soluciones creativas e innovadoras en desarrollo informático que ayuden a su empresa a crecer y desarrollarse rápidamente.')
@section('keywords', 'Desarrollo de sitios web, aplicaciones móviles, aplicaciones web, e-commerce, diseño gráfico')
{{-- For FB Meta tags --}}
@section('ogtitle', 'Desarrollo de sitios web modernos y compatibles con móviles | FDevelopers')
@section('ogurl', 'https://fdevelopershn.com/servicios')
@section('ogimage', 'https://fdevelopershn.com/img/servicios-web-laptop.webp')
@section('ogdescription', 'Desarrollo de sitios web utilizando lo último en tecnología de programación web.')
{{-- End For FB Meta tags --}}

@section('content')
  <div class="parallax-container text-center-img">
    <h1 class="center-align title-parallax-spacing font-weight-parallax">Nuestros Servicios</h1>
    <div class="parallax"><img src="img/servicios-web-laptop.webp" alt="servicios web laptop cel libreta de apuntes"></div>
  </div>
  <div class="background-white">
    <div class="content-wrapper">
      <div class="post container">
        <h5 class="center-align no-margin sub-title">Nuestros</h5>
        <h3 class="center-align color-darkgray title no-margin">Servicios de Desarrollo</h3>
        <div class="separate"></div>
        <p class="center-align color-darkgray">Ofrecemos una gama completa de servicios de desarrollo web y móvil. Nuestro
          objetivo es proporcionar soluciones creativas e innovadoras en desarrollo informático que ayuden a su empresa a crecer y
          desarrollarse rápidamente.</p>
        <div class="row icon-center-div center-align padding-top hide-on-small-and-down-icon-services">
          @foreach ($services as $service)
            <div class="col s6 l3 hide-on-small-and-down-row-padding">
              <img class="img-icon padding-bottom-icon" src="/img/services/{{ $service->image_service }}" alt="{{ $service->alt_image }}">
              <p class="no-margin color-darkgray">{{ $service->name_service }}</p>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
  <div class="background-gray">
    <div class="content-wrapper">
      <div class="post container">
        <div class="row">
          <div class="col s12 m12 l6">
            <div id="desarrollo-web-ser"></div>
          </div>
          <div class="col s12 m12 l6">
            <h3 class="center-align color-darkgray title no-margin padding-top">Desarrollo web</h3>
            <div class="separate"></div>
            <p class="justify-align color-darkgray">Estamos comprometidos con el desarrollo de sitios web modernos, profesionales,
            funcionales y compatibles con móviles.</p>
            <ul class="left-align">
              <li><i class="tiny material-icons">beenhere</i>Diseño Responsive</li>
              <li><i class="tiny material-icons">beenhere</i>Preparadas para SEO (Páginas web)</li>
              <li><i class="tiny material-icons">beenhere</i>Sitios Web Optimizados</li>
              <li><i class="tiny material-icons">beenhere</i>Base de Datos</li>
              <li><i class="tiny material-icons">beenhere</i>Últimas Tecnologías</li>
              <li><i class="tiny material-icons">beenhere</i>Diseño Alto Impacto</li>
              <li><i class="tiny material-icons">beenhere</i>Minimalista</li>
              <li><i class="tiny material-icons">beenhere</i>Compatibles con las navegadores más usados</li>
            </ul>
            <p class="justify-align color-darkgray">Las empresas hoy en día prefieren adoptar un sistema web que facilite sus procesos y
            mejore el tratamiento de sus productos.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="background-white">
    <div class="content-wrapper">
      <div class="post container">
        <h5 class="center-align no-margin sub-title">Nuestros</h5>
        <h3 class="center-align color-darkgray title no-margin">Paquetes Web</h3>
        <div class="separate"></div>
        <p class="justify-align color-darkgray">Nuestros paquetes web incluyen dominio .com .org o .net, hospedaje web con SSD para una mayor velocidad del sitio web
        y ancho de banda ilimitado para que puedas tener todas las visitas que necesites y tu sitio web no deje de funcionar.
        En caso de que necesites algo más complejo o personalizado contáctanos para ofrecerte un plan a la medida.
        </p>
        <div class="row">
          <div class="col s12 m6 l4">
            <div class="card hoverable large">
              <div class="card-image">
                <img src="img/book-laptop-html-css.webp" alt="paquete de desarrollo web startup">
                <span class="card-title">STARTUP</span>
              </div>
              <div class="card-content">
                <ul class="color-darkgray left-align">
                  <li><i class="tiny material-icons">web</i>1 a 4 páginas web.</li>
                  <li><i class="tiny material-icons">public</i>Dominio por un año.</li>
                  <li><i class="tiny material-icons">dns</i>Hospedaje web por un año.</li>
                  <li><i class="tiny material-icons">email</i>Cuentas de correo.</li>
                  <li><i class="tiny material-icons">devices</i>Sitio web adaptable a móviles.</li>
                  <li><i class="tiny material-icons">build</i>Soporte gratis durante 1 mes.</li>
                  <li><i class="tiny material-icons">security</i>Certificado de seguridad SSL.</li>
                  <li><i class="tiny material-icons">chrome_reader_mode</i>Formulario de contacto.</li>
                </ul>
              </div>
              <div class="card-action">
                <a href="/contactanos">Solicitar Información</a>
              </div>
            </div>
          </div>
          <div class="col s12 m6 l4">
            <div class="card hoverable large">
              <div class="card-image">
                <img src="img/book-laptop-html-css.webp" alt="paquete de desarrollo web pymes">
                <span class="card-title">PYMES</span>
              </div>
              <div class="card-content">
                <ul class="color-darkgray left-align">
                  <li><i class="tiny material-icons">web</i>5 a 8 páginas web.</li>
                  <li><i class="tiny material-icons">public</i>Dominio por un año.</li>
                  <li><i class="tiny material-icons">dns</i>Hospedaje web por un año.</li>
                  <li><i class="tiny material-icons">email</i>Cuentas de correo.</li>
                  <li><i class="tiny material-icons">devices</i>Sitio web adaptable a móviles.</li>
                  <li><i class="tiny material-icons">build</i>Soporte gratis durante 1 mes.</li>
                  <li><i class="tiny material-icons">security</i>Certificado de seguridad SSL.</li>
                  <li><i class="tiny material-icons">chrome_reader_mode</i>Formulario de contacto.</li>
                  <li><i class="tiny material-icons">show_chart</i>Google Analytics.</li>
                  <li><i class="tiny material-icons">search</i>SEO básico.</li>
                  <li><i class="tiny material-icons">create</i>Sitio autoadministrable.</li>
                  <li><i class="tiny material-icons">storage</i>Base de datos.</li>
                </ul>
              </div>
              <div class="card-action">
                <a href="/contactanos">Solicitar Información</a>
              </div>
            </div>
          </div>
          <div class="col s12 m6 l4">
            <div class="card hoverable large">
              <div class="card-image">
                <img src="img/book-laptop-html-css.webp" alt="paquete de desarrollo web corporativo">
                <span class="card-title">CORPORATIVO</span>
              </div>
              <div class="card-content">
                <ul class="color-darkgray left-align">
                  <li><i class="tiny material-icons">web</i>9 a 15 páginas web.</li>
                  <li><i class="tiny material-icons">public</i>Dominio por un año.</li>
                  <li><i class="tiny material-icons">dns</i>Hospedaje web por un año.</li>
                  <li><i class="tiny material-icons">email</i>Cuentas de correo.</li>
                  <li><i class="tiny material-icons">devices</i>Sitio web adaptable a móviles.</li>
                  <li><i class="tiny material-icons">build</i>Soporte gratis durante 1 mes.</li>
                  <li><i class="tiny material-icons">security</i>Certificado de seguridad SSL.</li>
                  <li><i class="tiny material-icons">chrome_reader_mode</i>Formulario de contacto.</li>
                  <li><i class="tiny material-icons">show_chart</i>Google Analytics.</li>
                  <li><i class="tiny material-icons">search</i>SEO básico.</li>
                  <li><i class="tiny material-icons">create</i>Sitio autoadministrable.</li>
                  <li><i class="tiny material-icons">storage</i>Base de datos.</li>
                  <li><i class="tiny material-icons">library_books</i>Blog.</li>
                  <li><i class="tiny material-icons">language</i>Dirección IP estática.</li>
                </ul>
              </div>
              <div class="card-action">
                <a href="/contactanos">Solicitar Información</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="background-gray">
    <div class="content-wrapper">
      <div class="post container">
        <div class="row">
          <div class="col s12 m12 l6">
            <h5 class="center-align no-margin sub-title padding-top">Desarrollo de</h5>
            <h3 class="center-align color-darkgray title no-margin">Apps Móviles</h3>
            <div class="separate"></div>
            <p class="center-align color-darkgray">Desarrollo de aplicaciones móviles a través de las tecnologías más utilizadas.</p>
            <ul class="left-align">
              <li><i class="tiny material-icons">beenhere</i>Plataforma Android e iOS</li>
              <li><i class="tiny material-icons">beenhere</i>Móviles y Tablets</li>
              <li><i class="tiny material-icons">beenhere</i>Hibridas y Nativas</li>
              <li><i class="tiny material-icons">beenhere</i>Conectividad</li>
              <li><i class="tiny material-icons">beenhere</i>Diseño Atractivo</li>
              <li><i class="tiny material-icons">beenhere</i>Últimas Tecnologías</li>
            </ul>
          </div>
          <div class="col s12 m12 l6">
            <div id="app-movil"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="background-white">
    <div class="content-wrapper">
      <div class="post container">
        <h5 class="center-align no-margin sub-title padding-top">Conoce nuestros</h5>
        <h3 class="center-align color-darkgray title no-margin">Otros Servicios</h3>
        <div class="separate"></div>
        <p class="center-align color-darkgray">Desarrollo de tiendas online (e-commerce) a la medida con sencillo panel de administración,
        ofrecemos también servicios de hosting compartido, marketing digital y diseño gráfico.</p>
        <div class="row icon-center-div center-align padding-top hide-on-small-and-down-icon-services">
            <div class="col s6 l3 hide-on-small-and-down-row-padding">
              <img class="img-icon padding-bottom-icon" src="img/e-commerce.webp" alt="Icono paginas web">
              <p class="no-margin color-darkgray">Comercio Electrónico</p>
            </div>
            <div class="col s6 l3 hide-on-small-and-down-row-padding">
              <img class="img-icon padding-bottom-icon" src="img/hosting.webp" alt="Icono maquetacion web">
              <p class="no-margin color-darkgray">Hosting Compartido</p>
            </div>
            <div class="col s6 l3 hide-on-small-and-down-row-padding">
              <img class="img-icon padding-bottom-icon" src="img/marketing.webp" alt="Icono aplicaciones web">
              <p class="no-margin color-darkgray">Marketing Digital</p>
            </div>
            <div class="col s6 l3 hide-on-small-and-down-row-padding">
              <img class="img-icon padding-bottom-icon" src="img/diseno.webp" alt="Icono desarrollo de aplicaciones para android">
              <p class="no-margin color-darkgray">Diseño Gráfico</p>
            </div>
        </div>
      </div>
    </div>
  </div>
  <div class="background-gray">
    <div class="content-wrapper">
      <div class="post container">
        <div class="row">
          <div class="col s12 m12 l6">
            <div id="e-commerce"></div>
          </div>
          <div class="col s12 m12 l6">
            <h5 class="center-align no-margin sub-title padding-top">Sitio web</h5>
            <h3 class="center-align color-darkgray title no-margin">E-commerce a medida</h3>
            <div class="separate"></div>
            <p class="justify-align color-darkgray">Comercio electrónico elaborado desde 0, o utilizando lo último en tecnología web (CMS),
            Tu decides.</p>
            <ul class="left-align">
              <li><i class="tiny material-icons">beenhere</i>Diseño atractivo</li>
              <li><i class="tiny material-icons">beenhere</i>Optimizado</li>
              <li><i class="tiny material-icons">beenhere</i>SEO básico</li>
              <li><i class="tiny material-icons">beenhere</i>Panel de administración sencilla</li>
              <li><i class="tiny material-icons">beenhere</i>Pasarela de pago</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="background-white">
    <div class="content-wrapper">
      <div class="post container">
        <h5 class="center-align no-margin sub-title padding-top">Social Media</h5>
        <h3 class="center-align color-darkgray title no-margin">Community Management</h3>
        <div class="separate"></div>
        <div class="row">
          <div class="col s12 m6 l4">
            <div class="card hoverable medium">
              <div class="card-image">
                <img src="img/marketing-digital.webp" alt="marketing digital manejo redes sociales plan junior">
                <span class="card-title">Plan Junior</span>
              </div>
              <div class="card-content">
                <ul class="color-darkgray left-align">
                  <li><i class="tiny material-icons">beenhere</i>1 post diario</li>
                  <li><i class="tiny material-icons">beenhere</i>Interacción en la redes.</li>
                  <li><i class="tiny material-icons">beenhere</i>Reporte mensual de las redes.</li>
                </ul>
              </div>
              <div class="card-action">
                <a href="/contactanos">Solicitar Información</a>
              </div>
            </div>
          </div>
          <div class="col s12 m6 l4">
            <div class="card hoverable medium">
              <div class="card-image">
                <img src="img/marketing-digital.webp" alt="marketing digital manejo redes sociales plan senior">
                <span class="card-title">Plan Senior</span>
              </div>
              <div class="card-content">
                <ul class="color-darkgray left-align">
                  <li><i class="tiny material-icons">beenhere</i>2 post diarios</li>
                  <li><i class="tiny material-icons">beenhere</i>Interacción en la redes.</li>
                  <li><i class="tiny material-icons">beenhere</i>Reporte mensual de las redes.</li>
                  <li><i class="tiny material-icons">beenhere</i>Uso de bots en Messenger.</li>
                </ul>
              </div>
              <div class="card-action">
                <a href="/contactanos">Solicitar Información</a>
              </div>
            </div>
          </div>
          <div class="col s12 m6 l4">
            <div class="card hoverable medium">
              <div class="card-image">
                <img src="img/marketing-digital.webp" alt="marketing digital manejo redes sociales plan master">
                <span class="card-title">Plan Master</span>
              </div>
              <div class="card-content">
                <ul class="color-darkgray left-align">
                  <li><i class="tiny material-icons">beenhere</i>1 post diario</li>
                  <li><i class="tiny material-icons">beenhere</i>Interacción en la redes.</li>
                  <li><i class="tiny material-icons">beenhere</i>Reporte mensual de las redes.</li>
                  <li><i class="tiny material-icons">beenhere</i>Uso de bots en Messenger.</li>
                  <li><i class="tiny material-icons">beenhere</i>Estrategia de Marketing.</li>
                </ul>
                </ul>
                </ul>
              </div>
              <div class="card-action">
                <a href="/contactanos">Solicitar Información</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
