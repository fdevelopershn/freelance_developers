@if ($errors->any())
<div id="ERROR_COPY" style="display:none" >
<div style="width:450px;height:70px;position: absolute;background-color:white;z-index:5">
    <ul>
    @foreach ($errors->all() as $error)
        <li >{{$error }}</li>
    @endforeach
  </ul>
  </div>
</div>
@endif

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.10/dist/sweetalert2.min.css">

    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.10/dist/sweetalert2.min.js">
    </script>
    <script src="{!! asset('js/admin-manager/jquery.min.js') !!}"></script>
    <script>


if ({{ $errors->any()}}) {
  swal({
    title:'Information',
    type: 'warning',
    html: jQuery('#ERROR_COPY').html(),
    showConfirmButton:true,
  });
}


</script>
