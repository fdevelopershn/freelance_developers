<script src="{!! asset('js/admin-manager/jquery.min.js') !!}"></script>
<script src="{!! asset('js/admin-manager/materialize.min.js') !!}"></script>


<script>
 /* Up Images */   
jQuery(function ()
{
    jQuery("input[id=fileuser]").change(function() {
        readURL(this);
    });
 
    const readURL = (input) => {

        if (input.files && input.files[0]) {
            const reader = new FileReader();
 
            reader.onload = (e) => {
                jQuery('#logo-img').attr('src', e.target.result)
                jQuery('#container-logo').css('display', 'block');
            }
            reader.readAsDataURL(input.files[0]);
        }
    };
})

jQuery(function ()
{
    jQuery("input[id=fileimage]").change(function() {
        readURL(this);
    });
 
    const readURL = (input) => {

        if (input.files && input.files[0]) {
            const reader = new FileReader();
 
            reader.onload = (e) => {
                jQuery('#file-img').attr('src', e.target.result)
                jQuery('#container-file').css('display', 'block');
            }
            reader.readAsDataURL(input.files[0]);
        }
    };
})

 
</script>
<!-- Initialization script -->
<script type="text/javascript">
$(document).ready(function(){
    $('select').formSelect();
  });

$(document).ready(function(){
  $('.sidenav').sidenav();
});

$(document).ready(function(){
  $('.collapsible').collapsible();
});

$(".dropdown-trigger").dropdown();


</script>
