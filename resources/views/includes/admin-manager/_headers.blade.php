@guest

@else
@include('includes.admin-manager._variables')
<div class="navbar-fixed">
  <nav style="background: linear-gradient(to bottom left, #006699 -1%, #3399ff 100%);" class="navbar">
    <div class="nav-wrapper">
      <a href="/fwasdevelopers/admin-manager" class="brand-logo white-text text-darken-4">Admin Panel</a>
      <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons white-text">menu</i></a>
    </div>
  </nav>
</div>
<ul id="slide-out" class="sidenav sidenav-fixed">

  <li><div class="user-view">
    <div class="background">
      @if (Auth::user()->image_background=="")
        <img style="width:400px" src="/img/backgrounds/mar.jpg">
        @else
          <img style="width:400px" src="/img/backgrounds/{{ Auth::user()->image_background }}">
      @endif
    </div>
    <a href="/fwasdevelopers/admin-manager/users/edit/{{ Auth::user()->id }}">
      @if (Auth::user()->image_user=="")
        <img class="circle" src="/img/users/user.png">
        @else
        <img class="circle" src="/img/users/{{ Auth::user()->image_user }}">
        @endif
  
    <span class="info-user white-text">
      @if (Auth::user()->type_user==1)
        Admin:
      @endif
      @if (Auth::user()->type_user==2)
        User:
      @endif
      </span>
    <span class="info-user white-text">{{ Auth::user()->name }}</span>
    <a href="#email"><span class="white-text email">{{ Auth::user()->email }}</span></a>
  </a>
  </div></li>
@php
if (Auth::user()->type_user==1) {
$person="Admin";
}
if (Auth::user()->type_user==2) {
$person="User";
}
@endphp
  <li><a><i class="material-icons black-text">cloud</i><span  class="bold-collapsible"><span class="blue-text">Admin</span> Panel < <span class="blue-text">F</span> ></span></a></li>
  <li><div class="divider"></div></li>
  <ul class="collapsible">
    @for ($x=0; $x < 2; $x++)

    <li>
      @if (Auth::user()->type_user==1 ||(Auth::user()->type_user==2 && $x==0 ))
      <div class="collapsible-header  waves-light bold-collapsible"><i class="material-icons">{{navrighticons[$x]}}</i>{{navright[$x]}}</div>
      @endif
      <div class="collapsible-body">
        <ul>
          @for ($y=0; $y < 6; $y++)
            @if (Auth::user()->type_user==1)
              @if ($x==0 && $y<6) 
              <li><a href="{{subnavrightref[$x][$y]}}" class="waves-effect">{{subnavright[$x][$y]}}<i class="material-icons">{{subnavrighticons[$x][$y]}}</i></a></li>
              @endif
              @if ($x==1 && $y<1) 
                <li><a href="{{subnavrightref[$x][$y]}}" class="waves-effect">{{subnavright[$x][$y]}}<i class="material-icons">{{subnavrighticons[$x][$y]}}</i></a></li>
              @endif 
              @elseif (Auth::user()->type_user==2)
              @if ($x==0 && $y<6 && $y>=0 && $y!=0)
                <li><a href="{{subnavrightref[$x][$y]}}" class="waves-effect">{{subnavright[$x][$y]}}<i class="material-icons">{{subnavrighticons[$x][$y]}}</i></a></li>
              @endif     
             
            @endif
          @endfor
        </ul>
      </div>
  </li>
    @endfor
  </ul>

  <ul class="collapsible">
    <li>
      <div class="collapsible-header waves-light bold-collapsible"><i class="material-icons">place</i>Account</div>
      <div class="collapsible-body">
        <ul>
          <li><a style="color:black" class="dropdown-item" href="/fwasdevelopers/admin-manager/users/edit/{{ Auth::user()->id }}"
            > Edit Profile
            <i class="material-icons">person</i>
          </a></li>
          <li><a style="color:black" class="dropdown-item" href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();"> {{ __('Logout') }}
            <i class="material-icons">settings</i>
          </a></li>
        </ul>
      </div>
    </li>
  </ul>
</ul>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
  @csrf
</form>

  @endguest
