@php
//Fwasdev Admin-Manager Sidenav
const navright = array("Pages","Manager-Projects");
const navrighticons = array("web","devices");

const subnavright = array(
  array("Users","Services","Projects","Sliders","Detail Sliders","Posts"),
  array("App Movil")
);

const subnavrightref = array(
  array("/fwasdevelopers/admin-manager/users",
  "/fwasdevelopers/admin-manager/services",
  "/fwasdevelopers/admin-manager/projects",
  "/fwasdevelopers/admin-manager/sliders",
  "/fwasdevelopers/admin-manager/detail-sliders",
  "/fwasdevelopers/admin-manager/posts",
),
 
  array("/fwasdevelopers/admin-manager/manager-projects/app-mobile",)
);

const subnavrighticons = array(
  array("person","dashboard","code","burst_mode","collections","collections"),
  array("developer_mode")
);
@endphp
