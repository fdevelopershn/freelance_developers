@if ($message = Session::get('edited'))
    <div id="message" style="display:none">
      <div id="information">
        <p>{{ Session::get('useredited') }}</p>
    </div>
    </div>
@endif

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.10/dist/sweetalert2.min.css">
<style media="screen">

.swal2-popup {
    width: 32em !important;
    height: 13rem !important;
}

.swal2-icon {
    margin: -0.75em 2.2em -7.125em 20.2em !important;
}
#information{
    width: 274px;
    height: 58px;
    position: absolute;
    background-color: white;
    z-index: 5;
    margin-left: -17px;
    margin-top: -13px;
}

.swal2-popup .swal2-content {
  width: 0px !important;
}

.swal2-popup .swal2-actions {
    margin: -0.25em auto -33px 2px !important;
}

</style>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.10/dist/sweetalert2.min.js">
    </script>
    <script src="{!! asset('js/admin-manager/jquery.min.js') !!}"></script>
    <script>

    if ({{ $message }}===1) {
      swal({
        title:'Information',
        type: 'success',
        html: jQuery('#message').html(),
        showConfirmButton:true,
      });
    }




</script>
