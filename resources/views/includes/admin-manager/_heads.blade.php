<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="msapplication-tap-highlight" content="no">
<meta name="description" content="">
<title>Freelance WAS Developers - Admin</title>
<!-- Materialize-->
<link rel="stylesheet" href="{!! asset('css/admin-manager/materialize.min.css') !!}">
<link rel="stylesheet" href="{!! asset('css/admin-manager/admin-manager.css') !!}">
<!-- Material Icons-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>