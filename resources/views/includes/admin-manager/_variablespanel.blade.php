@php
//Fwasdev Admin-Manager Panel
const subnavrightpanel = array("Users","Services","Projects","Sliders","Detail Sliders","Posts");

const subnavrightrefpanel =
  array("/fwasdevelopers/admin-manager/users",
  "/fwasdevelopers/admin-manager/services",
  "/fwasdevelopers/admin-manager/projects",
  "/fwasdevelopers/admin-manager/sliders",
  "/fwasdevelopers/admin-manager/detail-sliders",
  "/fwasdevelopers/admin-manager/posts",
  );

const subnavrighticonspanel = array("person","developer_board","code","burst_mode","collections","collections");

//Apps Mobile
const appmobile = array("Convert Currency");

const appmobileref =
  array("/fwasdevelopers/admin-manager/manager-projects/app-mobile/convert-currency");

const appmobileicons = array("monetization_on");

@endphp
