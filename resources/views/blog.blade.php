@extends('layouts.freelance')

@section('title', 'Blog sobre tecnología, internet y programación')
@section('urlcanonical','https://fdevelopershn.com/blog')
@section('description', 'Mantente actualizado con lo que sucede en el mundo informático a través de nuestro Blog
donde compartimos lo mejor sobre tecnología, internet y programación.')
@section('keywords', 'blog, tecnología, internet, programación')
{{-- For FB Meta tags --}}
@section('ogtitle', 'Blog sobre tecnología, internet y programación | FDevelopers')
@section('ogurl', 'https://fdevelopershn.com/blog')
@section('ogimage', 'https://fdevelopershn.com/img/blog-web-freelance.webp')
@section('ogdescription', 'Blog sobre tecnología, internet y programación')
{{-- End For FB Meta tags --}}

@section('content')
<div class="parallax-container text-center-img">
  <div class="row">
    <div class="col l12">
      <h1 class="center-align title-parallax-spacing bold-content">Blog</h1>
      <p class="center-align p-content">Tecnología, Internet y programación.</p>
    </div>
  </div>
  <div class="parallax"><img src="img/blog-web-freelance.webp" alt="blog web freelance developers"></div>
</div>
<div class="background-white">
  <div class="content-wrapper">
    <div class="post container">
      <div class="row">
        @foreach ($posts as $post)
        <div class="col s12 m6 l4">
          <a href="/blog/{{ $post->slug }}">
            <div class="card-blog medium hoverable">
              <div class="card-blog-image">
                <img src="img/blog/{{ $post->image }}">
                <div class="date">
                  <div class="day">{{ strftime("%d",strtotime($post->created_at)) }}</div>
                  <div class="month">{{ strftime("%b",strtotime($post->created_at)) }}</div>
                </div>
                <h2 class="card-blog-title">{{ $post->heading }}</h2>
              </div>
              <div class="card-blog-content">
                <h2>{{ $post->title }}</h2>
                {{ strip_tags(html_entity_decode(str_limit( $post->content, 90))) }}
                <div class="card-blog-action">
                  <p class="card-blog-p"><i class="tiny material-icons">access_time</i>
                    {{ ($post->created_at)->diffForHumans() }}</p>
                </div>
              </div>
            </div>
          </a>
        </div>
        @endforeach
      </div>
    </div>
  </div>
</div>
@endsection
