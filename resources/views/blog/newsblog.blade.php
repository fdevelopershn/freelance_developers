@extends('layouts.freelance')

@section('title') {{ $post->title }}@endsection
@section('urlcanonical','https://fdevelopershn.com/blog')
@section('description'){!! strip_tags(html_entity_decode(str_limit( $post->content, 82))) !!}@endsection
@section('keywords', 'blog, tecnología, internet, programación')

{{-- For FB Meta tags --}}
@section('ogtitle'){!! $post->title !!} | FDevelopers @endsection
@section('ogurl')https://fdevelopershn.com/blog/{{ $post->slug }} @endsection
@section('ogimage')https://fdevelopershn.com/img/blog/{{ $post->image }}@endsection
@section('ogdescription'){!! strip_tags(html_entity_decode(str_limit( $post->content, 82))) !!}@endsection
{{-- End For FB Meta tags --}}

@section('content')
<div class="background-white">
  <div class="container">
    <div class="row">
      <div class="col s12 m12 l12">
        <div class="card-blog">
          <div class="card-blog-image">
            <img src="/img/blog/{{ $post->image }}" alt="{{ $post->heading  }}">
            <h1 class="card-blog-title bold-title background-title hide-on-small-only">{{ $post->heading }}</h1>
          </div>
          <div class="card-blog-content">
            <div class="post-info">
              <span>{{ $post->created_at->format('l j F, Y') }}</span>
              <span>Por: {{ $post->author }}</span>
            </div>
            <h2>{{ $post->title }}</h2>
            {!!html_entity_decode($post->content) !!}
          </div>
          <div class="card-blog-action">
            <div class="fb-share-button" data-href="http://prueba.fdevelopershn.com/blog/{{ $post->slug }}"
              data-layout="button" data-size="large">
              <a style="margin-bottom:5px" target="_blank"
                href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fprueba.fdevelopershn.com%2Fblog%2F{{ $post->slug }}&amp;src=sdkpreparse"
                class="fb-xfbml-parse-ignore">Compartir</a></div>
            <a href="https://twitter.com/share?ref_src=twsrc%5Etfw" class="twitter-share-button" data-show-count="false"
              data-size="large">Tweet</a>
            <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
          </div>
        </div>
      </div>
    </div>
    <div id="fb-root"></div>
    <div class="fb-comments" mobile="data-mobile" data-href="http://prueba.fdevelopershn.com/blog/{{ $post->slug }}"
      data-numposts="100"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v3.2">
    </script>
  </div>
</div>
@endsection
