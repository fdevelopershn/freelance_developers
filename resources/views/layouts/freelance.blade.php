<!DOCTYPE html>
<html lang="es" dir="ltr">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>@yield('title')</title>
  <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
  <link rel="canonical" href="@yield('urlcanonical')"/>
  <link rel="stylesheet" href="{{asset('/css/animate.css')}}" type="text/css" />
  <link rel="stylesheet" href="{{asset('/css/style.css')}}" type="text/css" />
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />
  <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,900&display=swap" rel="stylesheet">

  <meta name="twitter:card" content="summary" />
  <meta name="description" content="@yield('description')">
  <meta name="keywords" content="@yield('keywords')">
  <meta name="author" content="Freelance Developers">
  <meta property="og:title" content="@yield('ogtitle')" />
  <meta property="og:type" content="@yield('ogtype', 'website')" />
  <meta property="og:url" content="@yield('ogurl')" />
  <meta property="og:image" content="@yield('ogimage')" />
  <meta property="og:site_name" content="Freelance Developers | Innovación y Calidad Garantizada" />
  <meta property="og:description" content="@yield('ogdescription')" />
  <script type="application/ld+json">{"@context":"http://schema.org","@type":"Organization","name":"Freelance Developers","url":"https://fdevelopershn.com/","address":"La Lima, Cortés, Honduras C.A","sameAs":["https://www.facebook.com/FDevelopersHN","https://twitter.com/HnDevelopers"]}</script>

  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-144094794-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-144094794-1');
  </script>
</head>

<body>
  <div class="navbar-fixed">
    <nav>
      <div class="container nav-wrapper">
        <a href="/" class="brand-logo hide-on-small-and-down-logo">Logo</a>
        <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        <ul class="right hide-on-med-and-down">
          <li class="active"><a href="/">Inicio</a></li>
          <li><a href="/nosotros">Nosotros</a></li>
          <li><a href="/servicios">Servicios</a></li>
          <li><a href="/portafolio">Portafolio</a></li>
          <li><a href="/blog">Blog</a></li>
          <li><a href="/contactanos">Contáctanos</a></li>
        </ul>
      </div>
    </nav>
  </div>
  <ul class="sidenav" id="mobile-demo">
    <li>
      <div class="user-view">
        <div class="background">
          <img src="/img/freelance-desarrollo-web-movil.webp" alt="freelance-desarrollo-web-movil">
        </div>
        <img src="/img/logo.png" alt="Logo freelance">
        <span class="color-darkgray name">Freelance Developers</span>
        <span class="color-darkgray email">fdevelopershn@gmail.com</span>
      </div>
    </li>
    <li><a href="/"><i class="material-icons">home</i>Inicio</a></li>
    <li><a href="/nosotros"><i class="material-icons">person</i>Nosotros</a></li>
    <li><a href="/servicios"><i class="material-icons">devices</i>Servicios</a></li>
    <li><a href="/portafolio"><i class="material-icons">work</i>Portafolio</a></li>
    <li><a href="/blog"><i class="material-icons">library_books</i>Blog</a></li>
    <li><a href="/contactanos"><i class="material-icons">email</i>Contáctanos</a></li>
  </ul>
  @yield('content')
  <script src="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js" data-cfasync="false"></script>
  <script>
    window.addEventListener("load", function(){
  window.cookieconsent.initialise({
    "palette": {
      "popup": {
        "background": "#01090f",
        "text": "#fffcfc"
      },
      "button": {
        "background": "#257ebf",
        "text": "#ffffff"
      }
    },
    "content": {
      "message": "Este sitio web utiliza cookies para garantizar que obtenga la mejor experiencia en nuestro sitio web.",
      "dismiss": " Lo tengo!",
      "link": "Aprende más"
    }
  })});
  </script>
  <script type="text/javascript" src="/js/jquery.min.js"></script>
  <script type="text/javascript" src="/js/main.min.js"></script>
  <script type="text/javascript" src="/js/freelance.min.js"></script>
  <script type="text/javascript">
    jQuery(document).ready(function() {
      	jQuery('.post').addClass("hidden").viewportChecker({
      	    classToAdd: 'visible animated fadeInDown', // Class to add to the elements when they are visible
      	    offset: 100
      	   });
      });
  </script>
  @yield('js')

  <script type="text/javascript">
    $(document).ready(function(){
        $('.parallax').parallax();
        $('.sidenav').sidenav();
        $('.slider').slider();
        $('.chips').chips();
        $('.tabs').tabs();
        @yield('script')
      });
  </script>
  <section class="footer">
    <footer class="page-footer">
        <div class="container">
          <div class="row">
            <div class="col s12 l6">
              <h4 class="white-text bold-title">Freelance Developers</h4>
              <p>Desarrollamos diferentes alternativas ajustadas a la necesidad de cada cliente en particular.</p>
              <p class="white-text left-align bold-title">¿Tienes un proyecto en mente?</p>
              <div class="left-align"><a href="/contactanos"><button class="btn-footer btn-footer">Contáctanos</button></a></div>
            </div>
            <div class="col s12 l4 offset-l2">
              <h4 class="white-text bold-title">Contacto</h4>
              <p class="left-align"><img class="social-media-s" src="/img/whatsapp.png" alt="Icono whatsapp"> (+504)
                9860-2471</p>
              <p class="left-align"><img class="social-media-s" src="/img/correo.png" alt="Icono correo">
                info@fdevelopershn.com</p>
              <p class="left-align"><img class="social-media-s" src="/img/ubicacion.png" alt="Icono ubicacion"> La Lima,
                Cortés, Honduras, C.A.</p>
              <div class="social-media-padding-top">
                <a href="https://www.facebook.com/FDevelopersHN"><img class="social-media-m" src="/img/facebook-logo.png"
                    alt="Icono facebook"></a>
                <a href="https://twitter.com/HnDevelopers"><img class="social-media-m" src="/img/twitter.png"
                    alt="Icono twitter"></a>
                <a href="https://www.instagram.com/FDevelopersHN/"><img class="social-media-m" src="/img/instagram.png"
                    alt="Icono instagram"></a>
              </div>
            </div>
          </div>
        </div>
        <div class="footer-copyright">
          <div class="container center-align">
            <span class="copyright">© 2019 Freelance Developers S. de R.L. de C.V | Todos los derechos reservados</span>
          </div>
        </div>
    </footer>
  </section>
</body>
</html>
