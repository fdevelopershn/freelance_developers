<!DOCTYPE html>
<html lang="en">

<head>
  @include('includes.admin-manager._heads')
</head>

<body class="has-fixed-sidenav">
  <header>
    @include('includes.admin-manager._headers')
  </header>

  <main>
    <div class="container">
      @yield('content')
    </div>
  </main>

  @include('includes.admin-manager._scripts')
</body>

</html>