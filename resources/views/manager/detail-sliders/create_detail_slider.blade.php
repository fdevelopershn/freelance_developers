@extends('layouts.admin-manager')

@section('content')
  <div class="row">
    <h2>Create Detail Slider <span class="blue-text"><</span>F<span class="blue-text">></span></h2>

    <form class="col s12" action="{{ URL('/fwasdevelopers/admin-manager/detail-sliders/store') }}"
     method="post" enctype="multipart/form-data">
     @csrf
      <div class="row">
        <div class="col s6">
          <div class="input-field col s12">
        <select name="slider_id" class="validate">
        @foreach ($sliders as $slider)
            <option value="{{ $slider->id }}">{{ $slider->name_slider }}</option>
        @endforeach
        </select>
        <label>Name Slider</label>
        </div>
        </div>

        <div class="col s12">
          <div class="input-field col s12">
            <textarea name="titulo_slider" class="materialize-textarea"></textarea>
            <label for="textarea1">Titulo Slider</label>
          </div>
        </div>

        <div class="col s12">
          <div class="input-field col s12">
            <textarea name="description_slider" class="materialize-textarea"></textarea>
            <label for="textarea1">Description Slider</label>
          </div>
        </div>

        <div class="col s6">
            <div class="input-field col s12">
            <select name="text_position">
              <option value="center">center</option>
              <option value="right">right</option>
              <option value="left">left</option>    
            </select>
          <label>Text position in slider</label>
          </div>
        </div>

        <div class="col s12">
            <div class="input-field col s12">
              <textarea name="alt_image" class="materialize-textarea"></textarea>
              <label for="textarea1">Alt Image</label>
            </div>
        </div>

        <div class="col s12">
          <div class="input-field col s6">
            <input name="link_slider" type="text" class="validate">
            <label for="last_name">Link Slider</label>
          </div>
        </div>

      <div class="file-field input-field ">
          <div class="input-field col s6">
              <div id="container-logo"  >
                  <img class="img-responsive" width="143" style="border-radius:40px" id="logo-img" src="#" />
              </div>  
              <div class="btn hoverable">
              <span>Search Image</span>
              <input name="image_slider" id="fileuser" type="file">
            </div>
            <div class="file-path-wrapper ">
              <input  style="display:none" class="file-path validate" type="text">
            </div>
          </div>
      </div> 

            </div>
      <button class="btn green waves-effect waves-light radius-buttons hoverable" type="submit" name="action">Save
   <i class="material-icons right">send</i>
 </button>
 <a href="/fwasdevelopers/admin-manager/detail-sliders"
 class="btn waves-effect blue waves-light radius-buttons hoverable" type="submit" name="action">Back
<i class="material-icons right">arrow_back</i>
</a>
    </form>
@endsection
