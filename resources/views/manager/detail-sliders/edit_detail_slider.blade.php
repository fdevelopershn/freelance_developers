@extends('layouts.admin-manager')

@section('content')
  <div class="row">
    <h2>Edit Detail Slider <span class="blue-text"><</span>F<span class="blue-text">></span></h2>

    <form class="col s12" action="{{ URL('/fwasdevelopers/admin-manager/detail-sliders/update/'.$detail_slider->id) }}"
     method="post" enctype="multipart/form-data">
     {{ csrf_field() }}
      <div class="row">
        <div class="col s6">
          <div class="input-field col s12">
        <select name="slider_id">
          <option value="{{ $sliderunique->id}}">{{ $sliderunique->name_slider }}</option>
          @foreach ($sliders as $slider)
            @if ($detail_slider->id_slider!=$slider->id)
              <option value="{{ $slider->id }}">{{ $slider->name_slider }}</option>
            @endif
            @endforeach
        </select>
        <label>Name Slider</label>
        </div>
        </div>

        <div class="col s12">
          <div class="input-field col s12">
            <textarea name="titulo_slider" class="materialize-textarea">{{ $detail_slider->titulo_slider }}</textarea>
            <label for="textarea1">Titulo Slider</label>
          </div>
        </div>

        <div class="col s12">
          <div class="input-field col s12">
            <textarea name="description_slider" class="materialize-textarea">{{ $detail_slider->description_slider }}</textarea>
            <label for="textarea1">Description Slider</label>
          </div>
        </div>

        <div class="col s6">
            <div class="input-field col s12">
            <select name="text_position">
              <option value="{{ $detail_slider->text_position }}" selected>{{ $detail_slider->text_position }}</option>
              @if ($detail_slider->text_position!=="center")
               <option value="center">center</option>
               @endif   
              @if ($detail_slider->text_position!=="right")
              <option value="right">right</option>
               @endif   
              @if ($detail_slider->text_position!=="left")
              <option value="left">left</option>
              @endif        
            </select>
          <label>Text position in slider</label>
          </div>
          </div>

        <div class="col s12">
            <div class="input-field col s12">
              <textarea name="alt_image" class="materialize-textarea">{{ $detail_slider->alt_image }}</textarea>
              <label for="textarea1">Alt Image</label>
            </div>
        </div>

        <div class="col s12">
          <div class="input-field col s6">
            <input name="link_slider" type="text" value="{{ $detail_slider->link_slider }}" class="validate">
            <label for="last_name">Link Slider</label>
          </div>
        </div>

  
        <div class="file-field input-field ">
            <div class="input-field col s6">
                <div id="container-logo"  >
                    <img class="img-responsive" width="143" style="border-radius:40px" id="logo-img" src="/img/sliders/{{ $detail_slider->image_slider }}" />
                </div>  
                <div class="btn hoverable">
                <span>Search Image</span>
                <input name="image_slider" id="fileuser" type="file">
              </div>
              <div class="file-path-wrapper ">
                <input  style="display:none" class="file-path validate" type="text">
              </div>
            </div>
        </div> 
        
            </div>
      <button class="btn green waves-effect waves-light radius-buttons hoverable" type="submit" name="action">Save
   <i class="material-icons right">send</i>
 </button>
 <a href="/fwasdevelopers/admin-manager/detail-sliders"
 class="btn waves-effect blue waves-light radius-buttons hoverable" type="submit" name="action">Back
<i class="material-icons right">arrow_back</i>
</a>
    </form>
@endsection
