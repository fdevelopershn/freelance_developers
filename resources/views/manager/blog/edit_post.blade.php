@extends('layouts.admin-manager')

@section('content')
<div class="row">
  <h2>Edit Post <span class="blue-text">
      <</span>F <span class="blue-text">>
    </span></h2>

  <form class="col s12" action="{{ URL('/fwasdevelopers/admin-manager/posts/update/'.$post->id) }}" method="post"
    enctype="multipart/form-data">
    @csrf
    <div class="row">

      <div class="col s12">
        <div class="input-field col s6">
          <input name="heading_post" type="text" value="{{ $post->heading }}" class="validate">
          <label for="last_name">Heading Post</label>
        </div>
      </div>

      <div class="col s12">
        <div class="input-field col s6">
          <input name="title_post" type="text" value="{{ $post->title }}" class="validate">
          <label for="last_name">Title Post</label>
        </div>
      </div>

      <div class="col s12">
        <div class="input-field col s12">
          <p for="textarea1">Content Post</p>
          <textarea name="content_post" id="content_post" class="materialize-textarea">{{ $post->content }}</textarea>
        </div>
      </div>

      <div class="file-field input-field ">
        <div class="input-field col s6">
          <div id="container-logo">
            <img class="img-responsive" width="160" style="border-radius:40px;margin-top:20px" id="logo-img"
              src="/img/blog/{{ $post->image }}" />
          </div>
          <div class="btn hoverable">
            <span>Search Image</span>
            <input name="image_post" id="fileuser" type="file">
          </div>
          <div class="file-path-wrapper ">
            <input style="display:none" class="file-path validate" type="text">
          </div>
        </div>
      </div>


    </div>
    <button class="btn green waves-effect waves-light radius-buttons hoverable" type="submit" name="action">Save
      <i class="material-icons right">send</i>
    </button>
    <a href="/fwasdevelopers/admin-manager/posts" class="btn waves-effect blue waves-light radius-buttons hoverable"
      type="submit" name="action">Back
      <i class="material-icons right">arrow_back</i>
    </a>
  </form>
  <script>
    var editor_config = {
      path_absolute : "/",
      selector: "#content_post",
      plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern"
      ],
      toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
      relative_urls: false,
      file_browser_callback : function(field_name, url, type, win) {
        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
        var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
  
        var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
        if (type == 'image') {
          cmsURL = cmsURL + "&type=Images";
        } else {
          cmsURL = cmsURL + "&type=Files";
        }
  
        tinyMCE.activeEditor.windowManager.open({
          file : cmsURL,
          title : 'Filemanager',
          width : x * 0.8,
          height : y * 0.8,
          resizable : "yes",
          close_previous : "no"
        });
      }
    };
  
    tinymce.init(editor_config);
  </script>
  @endsection