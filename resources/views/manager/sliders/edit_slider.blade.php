@extends('layouts.admin-manager')

@section('content')
  <div class="row">
    <h2>Edit Slider <span class="blue-text"><</span>F<span class="blue-text">></span></h2>

    <form class="col s12" action="{{ URL('/fwasdevelopers/admin-manager/sliders/update/'.$slider->id) }}"
      method="post" enctype="multipart/form-data">
      {{ csrf_field() }}
      <div class="row">
        <div class="col s12">
          <div class="input-field col s6">
            <input name="name_slider" value="{{ $slider->name_slider }}" type="text" class="validate">
            <label for="last_name">Name Service</label>
          </div>
        </div>

</div>
<button class="btn green waves-effect waves-light radius-buttons hoverable" type="submit" name="action">Save
  <i class="material-icons right">send</i>
</button>
<a href="/fwasdevelopers/admin-manager/sliders"
class="btn waves-effect blue waves-light radius-buttons hoverable" type="submit" name="action">Back
<i class="material-icons right">arrow_back</i>
</a>
</form>
@endsection
