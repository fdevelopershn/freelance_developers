@extends('layouts.admin-manager')

@section('content')
  <div class="row">
    <h2>Services <span class="blue-text"><</span>F<span class="blue-text">></span></h2>

<div class="col s12 l4">
    <a href="/fwasdevelopers/admin-manager/services/create"
    class="waves-effect green darken-1 btn-small button-set radius-buttons hoverable">
    <i class="material-icons right">add</i>Add New Service</a>
</div>
<form enctype="multipart/form-data" action="{{ URL('/fwasdevelopers/admin-manager/services') }}" method="get" role="form">
  <div class="col s2.5 l2.5 search">
    <a href="/fwasdevelopers/admin-manager/services" class="waves-effect blue darken-1 btn-small radius-buttons hoverable">
      <i class="material-icons left">autorenew</i>Refresh</a>
  </div>
<div class="col s0.5 l0.5 search">
  <button class="waves-effect blue darken-1 btn-small radius-buttons hoverable" type="submit">
    <i class="material-icons">search</i></button>
</div>
<div class="col s4 l4 search">
 <input name="name" style="margin-top:-2%"  type="text" placeholder="Search" >
</div>
</form>
    <table class="center-tr">
      <thead>
        <tr>
          <th>#</th>
          <th>Name Services</th>
          <th>Image Services</th>
          <th>Edit</th>
          <th>Delete</th>
        </tr>
      </thead>

      <tbody>

          @foreach ($services as $key => $service)
              <tr>
            <td>{{ $key+1}}</td>
            <td>{{ $service->name_service }}</td>
            <td> <img class="image-admin" src="/img/services/{{ $service->image_service }}" alt=""> </td>
            <td>
              <a href="/fwasdevelopers/admin-manager/services/edit/{{ $service->id }}"
                class="waves-effect blue darken-1 btn-small radius-buttons hoverable">
                <i class="material-icons">colorize</i></a></td>
                  <td>
                <a onClick="return confirm('¿Estas seguro que deseas eliminar esta Publicacion?');" href="/fwasdevelopers/admin-manager/services/delete/{{ $service->id }}"
                  class="waves-effect red btn-small radius-buttons hoverable"><i class="material-icons">delete</i></a>
                </td>
                  </tr>
              @endforeach

          </tbody>
        </table>
{{ $services->render('manager.pagination_materialize') }}
      </div>
      <a href="/fwasdevelopers/admin-manager"
      class="btn waves-effect blue darken-1 waves-light right radius-buttons hoverable" type="submit" name="action">Back
     <i class="material-icons right">arrow_back</i>
     </a>
     @include('includes.admin-manager._sweetalertaction')
@endsection
