@extends('layouts.admin-manager')

@section('content')
  <div class="row">
    <h2>Create Project <span class="blue-text"><</span>F<span class="blue-text">></span></h2>

    <form class="col s12" action="{{ URL('/fwasdevelopers/admin-manager/projects/store') }}"
     method="post" enctype="multipart/form-data">
     {{ csrf_field() }}
      <div class="row">
        <div class="col s6">
          <div class="input-field col s12">
        <select name="service_id" class="validate">
        @foreach ($services as $service)
            <option value="{{ $service->id }}">{{ $service->name_service }}</option>
        @endforeach
        </select>
        <label>Name Service</label>
        </div>
        </div>

        <div class="col s12">
          <div class="input-field col s6">
            <input name="name_project" type="text" class="validate">
            <label for="last_name">Name Project</label>
          </div>
        </div>

        <div class="col s12">
            <div class="input-field col s6">
              <input name="alt_image" type="text" class="validate">
              <label for="last_name">Alt Image</label>
            </div>
        </div>

        <div class="col s12">
          <div class="input-field col s6">
            <input name="link_project" type="text" class="validate">
            <label for="last_name">Link Project</label>
          </div>
        </div>

        <div class="col s12">
          <div class="input-field col s12">
            <textarea name="content_project" class="materialize-textarea"></textarea>
            <label for="textarea1">Content Project</label>
          </div>
        </div>

        <div class="file-field input-field ">
            <div class="input-field col s6">
                <div id="container-logo"  >
                    <img class="img-responsive" width="143" style="border-radius:40px" id="logo-img" src="#" />
                </div>  
                <div class="btn hoverable">
                <span>Search Image</span>
                <input name="image_project" id="fileuser" type="file">
              </div>
              <div class="file-path-wrapper ">
                <input  style="display:none" class="file-path validate" type="text">
              </div>
            </div>
        </div> 

      </div>
      <button class="btn green waves-effect waves-light radius-buttons hoverable" type="submit" name="action">Save
   <i class="material-icons right">send</i>
 </button>
 <a href="/fwasdevelopers/admin-manager/projects"
 class="btn waves-effect blue waves-light radius-buttons hoverable" type="submit" name="action">Back
<i class="material-icons right">arrow_back</i>
</a>
    </form>
@endsection
