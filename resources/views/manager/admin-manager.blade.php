@extends('layouts.admin-manager')

@section('content')
@include('includes.admin-manager._variablespanel')
  <div class="row">
    <h2>Admin Panel <span class="blue-text"><</span>F<span class="blue-text">></span></h2>
    @for ($x = 0; $x < 6; $x++)
    @if ((Auth::user()->type_user==1 && $x>=0) || (Auth::user()->type_user==2 && $x!=0))
    <div class="col s12 m4 center-card-manager space-main">
      <a href="{{ subnavrightrefpanel[$x]}}">
        <div class="waves-effect card-panel center-card-manager hoverable">
          <i class="large material-icons ">{{ subnavrighticonspanel[$x]}}</i>
          <p>{{ subnavrightpanel[$x]}}</p>
          </div>
        </a>
      </div>
      @endif
    @endfor
@endsection
