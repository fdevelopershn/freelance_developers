@extends('layouts.admin-manager')

@section('content')
  <div class="row">
    <h2>Users <span class="blue-text"><</span>F<span class="blue-text">></span></h2>

    <div class="col s12 l4">
      <a href="/fwasdevelopers/admin-manager/users/create"
      class="waves-effect green darken-1 btn-small button-set radius-buttons hoverable">
      <i class="material-icons right">add</i>Add New User</a>
    </div>
    <form enctype="multipart/form-data" action="{{ URL('/fwasdevelopers/admin-manager/users') }}" method="get" role="form">
      <div class="col s2.5 l2.5 search">
        <a href="/fwasdevelopers/admin-manager/users" class="waves-effect blue darken-1 btn-small radius-buttons hoverable">
          <i class="material-icons left">autorenew</i>Refresh</a>
        </div>
        <div class="col s0.5 l0.5 search">
          <button class="waves-effect blue darken-1 btn-small radius-buttons hoverable" type="submit">
            <i class="material-icons">search</i></button>
          </div>
          <div class="col s4 l4 search">
            <input name="name" style="margin-top:-2%"  type="text" placeholder="Search" >
          </div>
        </form>
        <table class="center-tr">
          <thead>
            <tr>
              <th>#</th>
              <th>Name User</th>
              <th>Email User</th>
              <th>Type User</th>
              <th>Image User</th>
              <th>Edit</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>

            @foreach ($users as $key => $user)
              @if (Auth::user()->id!=$user->id)
                <tr>
                  <td>{{ $key+1}}</td>
                  <td>{{ $user->name }}</td>
                  <td>{{ $user->email }}</td>
                  @if ($user->type_user==1)
                    <td>Admin</td>
                  @elseif ($user->type_user==2)
                    <td>User</td>
                  @endif
                  @if ($user->image_user=="")
                    <td><img class="circle image-admin" src="/img/users/user.png" alt=""></td>
                  @else
                    <td> <img class="circle image-admin" src="/img/users/{{ $user->image_user }}" alt=""> </td>
                  @endif

                  <td>
                    <a href="/fwasdevelopers/admin-manager/users/edit/{{ $user->id }}"
                      class="waves-effect blue darken-1 btn-small radius-buttons hoverable">
                      <i class="material-icons">colorize</i></a></td>
                      <td>
                        <a onClick="return confirm('¿Estas seguro que deseas eliminar esta Publicacion?');"
                        href="/fwasdevelopers/admin-manager/users/delete/{{ $user->id }}"
                        class="waves-effect red btn-small radius-buttons hoverable"><i class="material-icons">delete</i></a>
                      </td>
                    </tr>
                  @endif
                @endforeach

              </tbody>
            </table>
            {{ $users->render('manager.pagination_materialize') }}
          </div>
          <a href="/fwasdevelopers/admin-manager"
          class="btn waves-effect blue darken-1 waves-light right radius-buttons hoverable" type="submit" name="action">Back
          <i class="material-icons right">arrow_back</i>
        </a>
        @include('includes.admin-manager._sweetalertaction')
      @endsection
