@extends('layouts.admin-manager')

@section('content')
  <div class="row">
    <h2>Create User <span class="blue-text"><</span>F<span class="blue-text">></span></h2>

    <form class="col s12" action="{{ URL('/fwasdevelopers/admin-manager/users/store') }}"
    method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="row">

        <div class="col s12">
            <div class="file-field input-field " >
              <div class="input-field col s12">
                  <img class="circle image_edit" id="logo-img"  style="margin:auto;margin-top:-40px;margin-bottom:20px" src="#" alt="">         
                <div class="btn hoverable" style="display: block;margin: auto;float: none;text-align: center;width: 130px;" >
                  <span>Image User</span>
                  <input name="image_user" type="file" id="fileuser">
                </div>
                <div class="file-path-wrapper ">
                  <input style="display:none" class="file-path validate" placeholder="Add new image of user" style="display: block;margin: auto;float: none;text-align: center;width: 200px;" type="text">
                </div>
              </div>
            </div>
          </div>

      <div class="col s12">
        <div class="input-field col s6">
          <input name="name" type="text" class="validate">
          <label for="last_name">Name User</label>
        </div>
      </div>

      <div class="col s12">
        <div class="input-field col s6">
          <input name="email" type="email" class="validate">
          <label for="last_name">Email User</label>
        </div>
      </div>

      <div class="col s12">
        <div class="input-field col s12">
          <textarea name="description_user" class="materialize-textarea"></textarea>
          <label for="textarea1">Description User</label>
        </div>
      </div>

      <div class="col s12">
        <div class="input-field col s12">
          <textarea name="content_user" class="materialize-textarea"></textarea>
          <label for="textarea1">Content User</label>
        </div>
      </div>

      <div class="col s6">
        <div class="input-field col s12">
          <select name="type_user" class="validate">
            <option value="1">Admin</option>
            <option value="2">User</option>
          </select>
          <label>Type User</label>
        </div>
      </div>


      <div class="col s12">
        <div class="input-field col s6">
          <input name="password" type="password" class="validate">
          <label for="last_name">Password User</label>
        </div>
      </div>

      
      <div class="col s12">
          <div class="file-field input-field ">
            <div class="input-field col s8">
                <div id="container-file"  >
                    <img class="img-responsive" width="150" style="border-radius:50px" id="file-img" src="#" />
                </div>
              <div class="btn hoverable">
                <span>Image Background</span>
                <input name="image_background" id="fileimage" type="file">
              </div>
              <div class="file-path-wrapper ">
                <input style="display: none" class="file-path validate" type="text">
              </div>
            </div>
          </div>
        </div>

    </div>
    <button class="btn green waves-effect waves-light radius-buttons hoverable" type="submit" name="action">Save
      <i class="material-icons right">send</i>
    </button>
    <a href="/fwasdevelopers/admin-manager/users"
    class="btn waves-effect blue waves-light radius-buttons hoverable" type="submit" name="action">Back
    <i class="material-icons right">arrow_back</i>
  </a>
</form>
@include('includes.admin-manager._sweetalert')
@endsection
