@extends('layouts.admin-manager')

@section('content')
  <div class="row">
    <h2>Edit User <span class="blue-text"><</span>F<span class="blue-text">></span></h2>

    <form class="col s12" action="{{ URL('/fwasdevelopers/admin-manager/users/update/'.$user->id) }}"
      method="post" enctype="multipart/form-data">
      {{ csrf_field() }}
      <div class="row">

        <div class="col s12">
          <div class="file-field input-field " >
            <div class="input-field col s12">
              @if ($user->image_user=="")
                <img class="circle image_edit" id="logo-img"  style="margin:auto;margin-top:-40px;margin-bottom:20px" src="/img/users/user.png" alt="">
                @else
                <img class="circle image_edit" id="logo-img"  style="margin:auto;margin-top:-40px;margin-bottom:20px" src="/img/users/{{ $user->image_user }}" alt="">
              @endif
              <div class="btn hoverable" style="display: block;margin: auto;float: none;text-align: center;width: 130px;" >
                <span>Image User</span>
                <input name="image_user" id="fileuser" type="file">
              </div>
              <div class="file-path-wrapper ">
                <input style="display:none" class="file-path validate" placeholder="Add new image of user" style="display: block;margin: auto;float: none;text-align: center;width: 200px;" type="text">
              </div>
            </div>
          </div>
        </div>

        <div class="col s12">
          <div class="input-field col s6">
            <input name="name" type="text" value="{{ $user->name }}" >
            <label for="last_name">Name User</label>
          </div>
        </div>

        <div class="col s12">
          <div class="input-field col s6">
            <input name="email" type="text" value="{{ $user->email }}" >
            <label for="last_name">Email User</label>
          </div>
        </div>

        <div class="col s12">
          <div class="input-field col s12">
            <textarea name="description_user" class="materialize-textarea">{{ $user->description_user }}</textarea>
            <label for="textarea1">Description User</label>
          </div>
        </div>

        <div class="col s12">
          <div class="input-field col s12">
            <textarea name="content_user" class="materialize-textarea">{{ $user->content_user }}</textarea>
            <label for="textarea1">Content User</label>
          </div>
        </div>

        @if (Auth::user()->type_user==1)
          <div class="col s6">
            <div class="input-field col s12">
              <select name="type_user" class="validate">
                @if ($user->type_user==1)
                  <option value="1">Admin</option>
                @endif
                @if ($user->type_user==2)
                  <option value="2">User</option>
                @endif
                @if ($user->type_user!=1)
                  <option value="1">Admin</option>
                @endif
                @if ($user->type_user!=2)
                  <option value="2">User</option>
                @endif
              </select>
              <label>Type User</label>
            </div>
          </div>
        @endif

        <div class="col s12">
          <div class="input-field col s6">
            <input name="password" type="password" >
            <label for="last_name">Change Password User</label>
          </div>
        </div>


        <div class="col s12">
          <div class="file-field input-field ">
            <div class="input-field col s8">
              @if ($user->image_background=="")
                <img class="image_edit" id="file-img" src="/img/backgrounds/mar.jpg" alt="">
                @else
                  <img class="image_edit" id="file-img" src="/img/backgrounds/{{ $user->image_background }}" alt="">
                  @endif
              <div class="btn hoverable">
                <span>Image Background</span>
                <input name="image_background" id="fileimage" type="file">
              </div>
              <div class="file-path-wrapper ">
                <input style="display: none" class="file-path validate" type="text">
              </div>
            </div>
          </div>
        </div>

      </div>
      <button class="btn green waves-effect waves-light radius-buttons hoverable" type="submit" name="action">Save
        <i class="material-icons right">send</i>
      </button>
      @if (Auth::user()->type_user==1)
        <a href="/fwasdevelopers/admin-manager/users"
        class="btn waves-effect blue waves-light radius-buttons hoverable" type="submit" name="action">Back
        <i class="material-icons right">arrow_back</i>
      </a>
    @elseif(Auth::user()->type_user==2)
      <a href="/fwasdevelopers/admin-manager"
      class="btn waves-effect blue waves-light radius-buttons hoverable" type="submit" name="action">Back
      <i class="material-icons right">arrow_back</i>
    </a>
  @endif
</form>
@include('includes.admin-manager._sweetalert')

@endsection
