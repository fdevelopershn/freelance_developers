@include('includes.admin-manager._heads')
@if ($paginator->hasPages())
  <ul class="pagination" role="navigation">
    {{-- Previous Page Link --}}
    @if ($paginator->onFirstPage())
      <li class="disabled mov-left"><a class="waves-effect blue waves-light btn prev-page-disabled" href="#!">
        <span><i class="material-icons left">chevron_left
        </i>Nothing</span></a></li>
    @else
      <li class="waves-effect mov-left hoverable"><a class="waves-effect blue waves-light btn prev-page"
        href="{{ $paginator->previousPageUrl() }}">
        <span><i class="material-icons left">chevron_left
        </i>Previus</span></a></li>
      @endif

      {{-- Next Page Link --}}
      @if ($paginator->hasMorePages())
        <li class="waves-effect mov-right  hoverable"><a class="waves-effect blue waves-light btn next-page"
          href="{{ $paginator->nextPageUrl() }}"><span class="next-page-cus">
            <i class="material-icons right">chevron_right</i>Next</span></a></li>
      @else
        <li class="disabled mov-right"><a class="waves-effect waves-light btn next-page-disabled"
          href="#!"> <span> <i class="material-icons right">chevron_right
          </i>Nothing</span></a></li>
      @endif
    </ul>
  @endif
  @include('includes.admin-manager._scripts')
