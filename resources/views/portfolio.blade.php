@extends('layouts.freelance')

@section('title', 'Portafolio Web | Freelance Developers')
@section('urlcanonical','https://fdevelopershn.com/portafolio')
@section('description', 'Todos y cada uno de los proyectos que se ven en nuestro portafolio web son desarrollados, siguiendo los lineamientos del cliente pero aportando ideas y recomendaciones.')
@section('keywords', 'desarrollo web, maquetación web, aplicaciones web, apps móviles')
{{-- For FB Meta tags --}}
@section('ogtitle', 'Diferentes alternativas en desarrollo web ajustadas a la necesidad de cada cliente | FDevelopers')
@section('ogurl', 'https://fdevelopershn.com/portafolio')
@section('ogimage', 'https://fdevelopershn.com/img/portafolio-web-freelance.webp')
@section('ogdescription', 'Nos tomamos muy en serio cada proyecto que se nos encomienda por muy pequeño que este sea.')
{{-- End For FB Meta tags --}}

@section('content')
<div class="parallax-container text-center-img">
  <h1 class="center-align title-parallax-spacing font-weight-parallax">Portafolio Web</h1>
  <div class="parallax"><img src="img/portafolio-web-freelance.webp" alt="portafolio web freelance developers"></div>
</div>
<div class="content-wrapper">
  <div class="post container">
    <div class="row">
      <h5 class="center-align no-margin sub-title padding-top">Nuestros</h5>
      <h3 class="center-align color-darkgray title no-margin">Trabajos realizados</h3>
      <div class="separate"></div>
      <div class="center-align margin-bottom">
        <ul class="filtering">
          <li class="li-filter color-darkgray active" data-filter="all">Todos</li>
          @foreach ($services as $service)
          <li class="li-filter color-darkgray" data-filter="{{ $service->id }}">{{ $service->name_service }}</li>
          @endforeach
        </ul>
      </div>
      <div class="filter">
        @foreach ($projects as $project)
        <div class="col s12 m6 l4 filtr-item" data-category="{{ $project->id_service }}">
          <div class="card small hoverable">
            <div class="card-image waves-effect waves-block waves-light">
              <img class="activator" src="/img/projects/{{ $project->image_project }}" alt="{{ $project->alt_image }}">
            </div>
            <div class="card-content-portfolio">
              <div class="border-title">
                <span class="card-title-portfolio"><a href="{{ $project->link_project }}">{{ $project->name_project }}<i
                      class="material-icons right">more_vert</i></a></span>
                @foreach ($services as $service)
                @if ($service->id==$project->id_service)
                <span class="tag-title">{{ $service->name_service }}</span>
                @endif
                @endforeach
              </div>
            </div>
            <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">{{ $project->name_project }}<i
                  class="material-icons right">close</i></span>
              <p>{{ $project->content_project }}</p>
            </div>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </div>
</div>
<div class="background-gray">
  <div class="content-wrapper">
    <div class="post container">
      <div class="row">
        <div class="col s12 m12 l6">
          <div id="free-dp"></div>
        </div>
        <div class="col s12 m12 l6">
          <h3 class="center-align color-darkgray title">Freelance Developers</h3>
          <div class="separate"></div>
          <p class="justify-align color-darkgray">Todos y cada uno de los proyectos que se ven en nuestro portafolio web son
          desarrollados, siguiendo los lineamientos del cliente pero aportando ideas y recomendaciones. Nos
          tomamos bastante en serio cada proyecto que se nos encomienda por muy pequeño que este sea.</p>
          <p class="justify-align color-darkgray">Esperamos pronto poder colocar también tu proyecto en nuestro portafolio.</p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content-wrapper">
  <div class="post container">
    <h3 class="center-align color-darkgray p-phrase">Consigue por fin desarrollar el proyecto de tus sueños</h3>
    <div class="center-align social-media-padding-top"><a href="/contactanos"><button class="button">Empezar ya</button></a></div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="js/jquery.filterizr.min.js"></script>
<script type="text/javascript" src="js/freelance.min.js"></script>
@endsection
@section('script')
$('.filter').filterizr();
@endsection
