<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../login/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Freelance WAS Developers - Login
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
  <!-- CSS Files -->
  <link href="../login/css/bootstrap.min.css" rel="stylesheet" />
  <link href="../login/css/now-ui-kit.css?v=1.2.0" rel="stylesheet" />

  <link href="../login/demo/demo.css" rel="stylesheet" />
</head>

<body class="login-page sidebar-collapse">

  <div class="page-header clear-filter" filter-color="blue">
    <div class="page-header-image" style="background-image:url(../login/img/login.jpg)"></div>
    <div class="content">
      <div class="container">
        <div class="col-md-4 ml-auto mr-auto">
          <div class="card card-login card-plain">
            <form class="form" method="POST" action="{{ route('login') }}">
              @csrf

                  <img style="width:100%;margin-bottom:-35%;margin-top:-35%" src="../login/img/logo.png" alt="">

              <div class="card-body">
                <div class="input-group no-border input-lg">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="now-ui-icons users_circle-08"></i>
                    </span>
                  </div>
                  <input placeholder="E-mail Address..." id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                  @if ($errors->has('email'))
                      <span class="invalid-feedback" role="alert">
                          <strong style="color:white">{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
                </div>
                <div class="input-group no-border input-lg">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="now-ui-icons text_caps-small"></i>
                    </span>
                  </div>
                  <input placeholder="Password..." id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                  @if ($errors->has('password'))
                      <span class="invalid-feedback" role="alert">
                          <strong style="color:white">{{ $errors->first('password') }}</strong>
                      </span>
                  @endif
                </div>

                      <div class="custom-control custom-checkbox" style="float:left;margin:5px 5px auto 10px">
                            <input type="checkbox" style="margin-top:5px;margin-bottom:5px" class="custom-control-input" id="chkbox">
                            <label class="custom-control-label"  for="chkbox">Show Password</label>
                        </div>
              </div>
              <div class="card-footer text-center">
                {{-- <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                <label class="form-check-label" for="remember">
                    {{ __('Remember Me') }}
                </label> --}}
                <button type="submit" class="btn btn-primary btn-round btn-lg btn-block">{{ __('Login') }} </button>
                {{-- <div class="pull-left">
                  <h6>
                    <a class="link" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                  </h6>

                </div> --}}

            </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <footer class="footer">
      <div class="container">
        <div class="copyright" id="copyright">
          &copy;
          <script>
            document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
          </script>, Designed by
          <a href="#" target="_blank">Freelance WAS Developers</a>
        </div>
      </div>
    </footer>
  </div>

  <script src="../login/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="../login/js/core/popper.min.js" type="text/javascript"></script>
  <script src="../login/js/core/bootstrap.min.js" type="text/javascript"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="../login/js/plugins/bootstrap-switch.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="../login/js/plugins/nouislider.min.js" type="text/javascript"></script>
  <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
  <script src="../login/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
  <script src="../login/js/now-ui-kit.js?v=1.2.0" type="text/javascript"></script>
</body>
<script>
$('#chkbox').on('change',function(event){
    if($('#chkbox').is(':checked')){
        // Convertimos el input de contraseña a texto.
        $('#password').get(0).type='text';
     // En caso contrario..
     } else {
        // Lo convertimos a contraseña.
        $('#password').get(0).type='password';
     }
});
</script>
</html>
