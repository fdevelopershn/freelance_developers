@extends('layouts.freelance')

@section('title', 'Diseño, desarrollo de páginas web y marketing digital en Honduras')
@section('urlcanonical','https://fdevelopershn.com/')
@section('description', 'Contamos con expertos en desarrollo web en Honduras, que pueden ayudarte a desarrollar sitios web modernos y atractivos a precios muy competitivos.')
@section('keywords', 'Freelance, desarrollo web, paginas web dinámicas, aplicaciones web, app móviles, servicios de
maquetación web')
{{-- For FB Meta tags --}}
@section('ogtitle', 'Desarrollo web, móvil y servicios de marketing digital en Honduras | FDevelopers')
@section('ogurl', 'https://fdevelopershn.com')
@section('ogimage', 'https://fdevelopershn.com/img/proceso-trabajo-laptop-book.webp')
@section('ogdescription', 'Desarrollo de sitios web, aplicaciones móviles y marketing digital en Honduras')
{{-- End For FB Meta tags --}}

@section('content')
<div class="slider">
  <ul class="slides">
    @foreach ($detail_sliders as $detail_slider)
    <li>
      <img src="/img/sliders/{{ $detail_slider->image_slider }}" alt="{{ $detail_slider->alt_image  }}">
      <!-- random image -->
      <div class="caption {{ $detail_slider->text_position }}-align">
        <h1 class="bold-title-slider">{{ $detail_slider->titulo_slider }}</h1>
        <p class="p-slider-content">{{ $detail_slider->description_slider }}</p>
      </div>
    </li>
    @endforeach
  </ul>
</div>
<div class="background-white">
  <div class="content-wrapper">
    <div class=" post container">
      <h5 class="center-align no-margin sub-title">Conoce más</h5>
      <h3 class="center-align color-darkgray title no-margin">Sobre Nosotros</h3>
      <div class="separate"></div>
      <div class="row">
        <div class="col s12 m12 l12">
          <p class="center-align color-darkgray">Somos una agencia altamente calificada y especializada en cada campo del desarrollo web,
          móvil y marketing digital en Honduras. Brindamos soluciones creativas e innovadoras, ofreciendo los mejores servicios en desarrollo
          informático, utilizando las últimas tecnologías en tendencias, con el fin de incrementar el rendimiento
          del proyecto y sobrepasar las expectativas de nuestros clientes.</p>
          <div class="center-align"><a href="/nosotros"><button class="button">Ver Más...</button></a></div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="background-gray">
  <div class="content-wrapper">
    <div class="post container">
      <div class="row">
        <div class="col s12 m12 l6">
          <div id="site-web"></div>
        </div>
        <div class="col s12 m12 l6">
          <h5 class="center-align no-margin sub-title padding-top">Importancia</h5>
          <h3 class="center-align color-darkgray title no-margin">Sitios web</h3>
          <div class="separate"></div>
          <p class="justify-align color-darkgray">Para una empresa, lo mejor es estar donde están los usuarios y estos, en su
          gran mayoría, están en línea. Este aumento en el uso de Internet presenta potencial para todos los tipos de empresas,
          siempre que puedan encontrar el momento en que su público está en línea.</p>
          <p class="justify-align color-darkgray">Freelance Developers cuenta con expertos que pueden desarrollar la estrategia
          que tu empresa tanto necesita, desde página web hasta redes sociales y proveer a tu negocio con todas las herramientas
          para convertirte en líder de la industria en la nueva era digital.</p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="background-white">
  <div class="content-wrapper">
    <div class="post container">
      <h5 class="center-align no-margin sub-title">Nuestros</h5>
      <h3 class="center-align color-darkgray title no-margin">Servicios de Desarrollo</h3>
      <div class="separate"></div>
      <p class="center-align color-darkgray">Ofrecemos una gama completa de servicios de desarrollo web y móvil. Nuestro
        objetivo es proporcionar soluciones creativas e innovadoras en desarrollo informático que ayuden a su empresa a crecer y
        desarrollarse rápidamente.</p>
      <div class="row icon-center-div center-align padding-top hide-on-small-and-down-icon-services">
        @foreach ($services as $service)
        <div class="col s6 l3 hide-on-small-and-down-row-padding">
          <img class="img-icon padding-bottom-icon" src="/img/services/{{ $service->image_service }}"
            alt="Icono desarrollo web">
          <p class="no-margin color-darkgray">{{ $service->name_service }}</p>
        </div>
        @endforeach
      </div>
      <div class="center-align padding-top"><a href="/servicios"><button class="button">Ver Más...</button></a></div>
    </div>
  </div>
</div>
<div class="background-gray">
  <div class="content-wrapper">
    <div class="post container">
      <div class="row">
        <div class="col s12 m12 l6">
          <h5 class="center-align no-margin sub-title">Sitios web</h5>
          <h3 class="center-align color-darkgray title no-margin">Adaptables a móviles</h3>
          <div class="separate"></div>
          <p class="justify-align color-darkgray">Cada vez más y más personas están visitando los sitios web desde un smartphone.
          Las web responsive se han convertido en el estándar de la industria, y los propietarios de sitios web no quieren
          quedarse atrás.</p>
          <p class="justify-align color-darkgray">El diseño web adaptable a móviles trata de proporcionar al usuario la mejor
          experiencia posible, no importa el dispositivo que esté utilizando. Se trata de hacer más fácil la navegación
          al usuario.</p>
          <p class="justify-align color-darkgray">En Freelance Developers podemos dar ese carácter moderno que tu empresa necesita
          para poder adaptarse a los consumidores de la actualidad.</p>
        </div>
        <div class="col s12 m12 l6">
          <div id="web-responsive"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="background-white">
  <div class="content-wrapper">
    <div class="post container">
      <h5 class="center-align no-margin sub-title">Nuestro</h5>
      <h3 class="center-align color-darkgray title no-margin">Portafolio</h3>
      <div class="separate"></div>
      <div class="row">
        @foreach ($projects as $project)
        <div class="col s12 m6 l4">
          <div class="card small hoverable">
            <div class="card-image waves-effect waves-block waves-light">
              <img class="activator" src="/img/projects/{{ $project->image_project }}"
                alt="computadora mac que muestra el sitio web de freelance">
            </div>
            <div class="card-content-portfolio">
              <div class="border-title">
                <span class="card-title-portfolio"><a href="{{ $project->link_project }}">{{ $project->name_project }}<i
                      class="material-icons right">more_vert</i></a></span>
                @foreach ($services as $service)
                @if ($service->id==$project->id_service)
                <span class="tag-title">{{ $service->name_service }}</span>
                @endif
                @endforeach
              </div>
            </div>
            <div class="card-reveal">
              <span class="card-title grey-text text-darken-4">{{ $project->name_project }}<i
                  class="material-icons right">close</i></span>
              <p>{{ $project->content_project }}</p>
            </div>
          </div>
        </div>
        @endforeach
      </div>
      <div class="center-align"><a href="/portafolio"><button class="button">Ver Más...</button></a></div>
    </div>
  </div>
</div>
<div class="background-gray">
  <div class="content-wrapper">
    <div class="post container">
      <div class="row">
        <div class="col s12 m12 l6">
          <div id="bene-freelance"></div>
        </div>
        <div class="col s12 m12 l6">
          <h3 class="left-align color-darkgray padding-top">¿Qué obtienes al adquirir un sitio web con Freelance Developers?</h3>
          <div class="separate"></div>
          <ul class="color-darkgray left-align">
            <li><i class="tiny material-icons">public</i>Dominio por un año.</li>
            <li><i class="tiny material-icons">dns</i>Hospedaje web por un año.</li>
            <li><i class="tiny material-icons">email</i>Cuentas de correo.</li>
            <li><i class="tiny material-icons">devices</i>Sitio web adaptable a móviles.</li>
            <li><i class="tiny material-icons">build</i>Soporte gratis durante 1 mes.</li>
            <li><i class="tiny material-icons">security</i>Certificado de seguridad SSL.</li>
            <li><i class="tiny material-icons">chrome_reader_mode</i>Formulario de contacto.</li>
            <li><i class="tiny material-icons">show_chart</i>Google Analytics.</li>
            <li><i class="tiny material-icons">search</i>SEO básico.</li>
            <li><i class="tiny material-icons">create</i>Sitio autoadministrable.</li>
            <li><i class="tiny material-icons">storage</i>Base de datos.</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="background-white">
  <div class="content-wrapper">
    <div class="post container">
      <h5 class="center-align no-margin sub-title">Últimas noticias</h5>
      <h2 class="center-align color-darkgray title no-margin">Nuestro blog</h2>
      <div class="separate"></div>
      <div class="row">
        @foreach ($posts as $post)
        <div class="col s12 m6 l4">
          <a href="/blog/{{ $post->slug }}">
            <div class="card-blog medium hoverable">
              <div class="card-blog-image">
                <img src="img/blog/{{ $post->image }}" alt="{{ $post->heading  }}">
                <div class="date">
                  <div class="day">{{ strftime("%d",strtotime($post->created_at)) }}</div>
                  <div class="month">{{ strftime("%b",strtotime($post->created_at)) }}</div>
                </div>
                <h2 class="card-blog-title">{{ $post->heading }}</h2>
              </div>
              <div class="card-blog-content">
                <h2>{{ $post->title }}</h2>
                {{ strip_tags(html_entity_decode(str_limit( $post->content, 90))) }}
                <div class="card-blog-action">
                  <p class="card-blog-p"><i class="tiny material-icons">access_time</i>
                    {{ ($post->created_at)->diffForHumans() }}</p>
                </div>
              </div>
            </div>
          </a>
        </div>
        @endforeach
      </div>
      <div class="center-align"><a href="/blog"><button class="button">Ver Más...</button></a></div>
    </div>
  </div>
</div>
@endsection
