@extends('layouts.freelance')

@section('title', 'Contáctanos | Freelance Developers')
@section('urlcanonical','https://fdevelopershn.com/contactanos')
@section('description', 'Cuéntanos acerca de tu proyecto para ofrecerte las mejores soluciones y servicios.
Si tienes alguna duda o quieres darnos alguna sugerencia o propuesta, ¡Escribenos sin compromiso!')
@section('keywords', 'contacto, proyecto web')
{{-- For FB Meta tags --}}
@section('ogtitle', 'Cuéntanos acerca de tu proyecto | FDevelopers')
@section('ogurl', 'https://fdevelopershn.com/contactanos')
@section('ogimage', 'https://fdevelopershn.com/img/contact-web-freelance.webp')
@section('ogdescription', 'Cuéntanos acerca de tu proyecto para ofrecerte las mejores soluciones y servicios')
{{-- End For FB Meta tags --}}

@section('content')
  <div class="parallax-container text-center-img">
    <h1 class="center-align title-parallax-spacing font-weight-parallax">Cuéntanos tu proyecto</h1>
    <div class="parallax"><img src="img/contact-web-freelance.webp" alt="contacto web freelance developers"></div>
  </div>
  <div class="content-wrapper">
    <div class="post container">
      <div class="background-white">
        <div class="padding-top">
          <p class="center-align color-darkgray">Cuéntanos acerca de tu proyecto para ofrecerte las mejores
          soluciones y servicios. Si tienes alguna duda o quieres darnos alguna sugerencia o propuesta,
          ¡Escribenos sin compromiso!</p>
          <p class="center-align color-darkgray bold-content">Para entrar en comunicación, por favor llene el formulario y nos
          pondremos en contacto lo más pronto posible.</p>
        </div>
        <div class="padding-top">
          <div class="row">
            <form class="col s12" action="{{ URL('/contactanos/store') }}"
            method="post" enctype="multipart/form-data">
            @csrf
            {!! RecaptchaV3::initJs() !!}
              <div class="post row">
                <div class="input-field col s12 m6 l6">
                  <i class="material-icons prefix">account_circle</i>
                  <input id="name" name="name" type="text" class="validate">
                  <label for="name">Nombre</label>
                </div>
                <div class="input-field col s12 m6 l6">
                  <i class="material-icons prefix">phone</i>
                  <input id="telephone" name="telephone" type="tel" class="validate">
                  <label for="telephone">Telefono</label>
                </div>
                <div class="input-field col s12 m6 l6">
                  <i class="material-icons prefix">email</i>
                  <input id="email" type="text" name="email" class="validate">
                  <label for="email">Email</label>
                </div>
                <div class="input-field col s12 m6 l6">
                  <i class="material-icons prefix">business</i>
                  <input id="company" name="company" type="text" class="validate">
                  <label for="company">Empresa</label>
                </div>
                <div class="input-field col s12 m12 l12">
                  <i class="material-icons prefix">create</i>
                  <textarea id="textarea1" name="message" class="materialize-textarea" data-length="120"></textarea>
                  <label for="textarea1">Cuéntanos sobre tu idea..</label>
                </div>
                <p class="center-align bold-content">¿Por que medio te enteraste de nosotros?</p>
                <div class="input-field col s12 m12 l12">
                  <i class="material-icons prefix">help</i>
                  <textarea id="textarea2" name="social" class="materialize-textarea" data-length="40" placeholder="Sitio Web, Facebook, Twitter, Amistades, Otro.."></textarea>
                </div>
              </div>

              <div class="post center-align">
                <button class="button" type="submit" name="action">Enviar Ahora
                <i class="material-icons right">send</i>
                </button>
              </div>
              {!! RecaptchaV3::field('contacto') !!}
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')
   $('input#input_text, textarea#textarea1').characterCounter();
   $('input#input_text, textarea#textarea2').characterCounter();
@endsection
