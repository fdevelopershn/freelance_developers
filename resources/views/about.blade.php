@extends('layouts.freelance')

@section('title', '¿Quiénes somos? | Freelance Developers')
@section('urlcanonical','https://fdevelopershn.com/nosotros')
@section('description', 'Somos una empresa altamente calificada y especializada en cada campo del desarrollo web, móvil
y marketing digital. Brindamos soluciones creativas e innovadoras, ofreciendo los mejores servicios en desarrollo informático.')
@section('keywords', ' Páginas web Honduras, sitios web dinámicos, desarrollo móvil, servicios de marketing digital')
{{-- For FB Meta tags --}}
@section('ogtitle', 'Descubre por que somos lo que necesitas, desarrollo web y móvil | FDevelopers')
@section('ogurl', 'https://fdevelopershn.com/nosotros')
@section('ogimage', 'https://fdevelopershn.com/img/team-acerca-de.webp')
@section('ogdescription', 'Los mejores servicios en desarrollo web, mobile y marketing digital en Honduras')
{{-- End For FB Meta tags --}}

@section('content')
  <div class="parallax-container text-center-img">
    <h1 class="center-align title-parallax-spacing font-weight-parallax">Freelance Developers</h1>
    <div class="parallax"><img src="/img/team-acerca-de.webp" alt="team acerca de freelance developers"></div>
  </div>
  <div class="content-wrapper">
    <div class="container">
      <div class="background-white">
          <div class="post">
            <div class="row">
              <div class="col s12">
                <h5 class="center-align no-margin sub-title">Conoce más</h5>
                <h2 class="center-align color-darkgray title no-margin">Sobre Nosotros</h2>
                <div class="separate"></div>
                <div class="row">
                    <div class="col l12">
                      <p class="center-align color-darkgray">Somos una agencia altamente calificada y especializada en cada campo del desarrollo web,
                      móvil y marketing digital en Honduras. Brindamos soluciones creativas e innovadoras, ofreciendo los mejores servicios en desarrollo
                      informático, utilizando las últimas tecnologías en tendencias, con el fin de incrementar el rendimiento
                      del proyecto y sobrepasar las expectativas de nuestros clientes.</p>
                    </div>
                </div>
                <div class="row">
                  <div class="col s12 m6 l4">
                    <div class="card hoverable medium">
                      <div class="card-image">
                        <img class="img-padding-full" src="/img/book-laptop-html-css.webp" alt="libro html css y laptop">
                      </div>
                      <div class="avatar">
                        <img class="img-avatar" src="/img/geo.png" alt="fotografia de uno de los fundadores">
                      </div>
                      <div class="card-content top-text-card-medium center-align">
                        <span class="card-profile-title color-darkgray">Gerson Alvarez</span>
                        <span class="card-profile-subtitle">Fundador/Front-end Developer</span>
                        <p class="card-profile-p color-darkgray">Ingeniero en Ciencias de la Computación.</p>
                        <div class="card-profile-icon">
                          <a href="https://www.facebook.com/gerson.alvarez.39"><img class="icon" src="/img/facebook-gray.png" alt="Icono facebook"></a>
                          <a href="https://www.linkedin.com/in/gerson-geovany-alvarez-flores-53a4b5162/"><img class="icon" src="/img/in.png" alt="Icono linkedin"></a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col s12 m6 l4">
                    <div class="card hoverable medium">
                      <div class="card-image">
                        <img class="img-padding-full" src="/img/book-laptop-html-css.webp" alt="libro html css y laptop">
                      </div>
                      <div class="avatar">
                        <img class="img-avatar" src="/img/alejandro.png" alt="fotografia de uno de los fundadores">
                      </div>
                      <div class="card-content top-text-card-medium center-align">
                        <span class="card-profile-title color-darkgray">Alejandro Tejada</span>
                        <span class="card-profile-subtitle">Fundador/Back-end Developer</span>
                        <p class="card-profile-p color-darkgray">Ingeniero en Ciencias de la Computación.</p>
                        <div class="card-profile-icon">
                          <a href="https://www.facebook.com/profile.php?id=100001285121518"><img class="icon" src="/img/facebook-gray.png" alt="Icono facebook"></a>
                          <a href="https://www.linkedin.com/in/jose-alejandro-tejada-guardado-029a61159/"><img class="icon" src="/img/in.png" alt="Icono linkedin"></a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col s12 m6 l4">
                    <div class="card hoverable medium">
                      <div class="card-image">
                        <img class="img-padding-full" src="/img/book-laptop-html-css.webp" alt="libro html css y laptop">
                      </div>
                      <div class="avatar">
                        <img class="img-avatar" src="/img/jonathan.png" alt="fotografia de uno de los fundadores">
                      </div>
                      <div class="card-content top-text-card-medium center-align">
                        <span class="card-profile-title color-darkgray">Jonathan Franco</span>
                        <span class="card-profile-subtitle">Fundador/Back-end Developer</span>
                        <p class="card-profile-p color-darkgray">Ingeniero en Ciencias de la Computación.</p>
                        <div class="card-profile-icon">
                          <a href="https://www.facebook.com/DavidP94"><img class="icon" src="/img/facebook-gray.png" alt="Icono facebook"></a>
                          <a href="https://www.linkedin.com/in/jonathan-d-franco-09848b43/"><img class="icon" src="/img/in.png" alt="Icono linkedin"></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
  <div class="background-gray">
    <div class="content-wrapper">
      <div class="post container">
        <div class="row">
          <div class="col s12 m12 l6">
            <div id="mis-vis"></div>
          </div>
          <div class="col s12 m12 l6">
            <section class="mision padding-top">
              <h3 class="center-align color-darkgray title no-margin">Misión</h3>
              <div class="separate"></div>
              <p class="justify-align">Brindar los mejores servicios en desarrollo informático a las empresas,
              emprendedores y autónomos con el fin de incrementar su competitividad en el mundo del internet, manteniendo una
              relación cercana y directa con el cliente ofreciendo las soluciones más innovadoras a precios muy competitivos.</p>
            </section>
            <section class="vision">
              <h3 class="center-align color-darkgray title no-margin">Visión</h3>
              <div class="separate"></div>
              <p class="justify-align">Ser una empresa confiable, sólida y rentable que brinde servicios en
              desarrollo informático de primera categoría, convirtiéndonos en la mejor alternativa a nivel nacional e
              internacional, garantizando calidad en cada servicio que brindemos a nuestros clientes.</p>
            </section>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="background-white">
    <div class="content-wrapper">
      <div class="post container">
        <div class="row">
          <div class="col s12 m12 l12">
            <h2 class="center-align color-darkgray title no-margin">Nuestros valores</h2>
            <div class="separate"></div>
            <p class="center-align color-darkgray bold-content">Pasión, Compromiso, Innovación, Calidad y Honestidad.</p>
            <p class="center-align color-darkgray">El éxito de nuestros clientes es también el nuestro, por eso damos más del 100%
            con cada proyecto que nos confían. Desde microempresas hasta corporaciones, nosotros podemos satisfacer las necesidades
            de su negocio.</p>
            <p class="center-align color-darkgray bold-content">"Innovación y Calidad Garantizada"</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="parallax-container hide-on-small-parallax-mobile">
    <div class="post padding-content-parallax">
      <h3 class="center-align white-text title">¿Cómo trabajamos?</h3>
      <div class="row container">
        <div class="col s6 m4 l2 hide-on-small-and-down-box-homework">
          <div class="process-box center-align">
            <i class="small material-icons">local_cafe</i>
            <p>Reunión</p>
          </div>
        </div>
        <div class="col s6 m4 l2 hide-on-small-and-down-box-homework">
          <div class="process-box center-align">
            <i class="small material-icons">lightbulb_outline</i>
            <p>Planificación</p>
          </div>
        </div>
        <div class="col s6 m4 l2 hide-on-small-and-down-box-homework">
          <div class="process-box center-align">
            <i class="small material-icons">color_lens</i>
            <p>Diseño</p>
          </div>
        </div>
        <div class="col s6 m4 l2 hide-on-small-and-down-box-homework">
          <div class="process-box center-align">
            <i class="small material-icons">developer_mode</i>
            <p>Desarrollo</p>
          </div>
        </div>
        <div class="col s6 m4 l2 hide-on-small-and-down-box-homework">
          <div class="process-box center-align">
            <i class="small material-icons">verified_user</i>
            <p>Pruebas</p>
          </div>
        </div>
        <div class="col s6 m4 l2 hide-on-small-and-down-box-homework">
          <div class="process-box center-align">
            <i class="small material-icons">archive</i>
            <p>Entrega</p>
          </div>
        </div>
      </div>
    </div>
    <div class="parallax"><img src="/img/proceso-trabajo-laptop-book.webp" alt="proceso de trabajo web, laptop y libro de apuntes"></div>
  </div>
  <div class="content-wrapper">
    <div class="post container">
      <h3 class="center-align color-darkgray p-phrase">Descubre por qué somos lo que necesitas.</h3>
      <div class="center-align social-media-padding-top"><a href="/contactanos"><button class="button">Empecemos tu Proyecto</button></a></div>
    </div>
  </div>
@endsection
